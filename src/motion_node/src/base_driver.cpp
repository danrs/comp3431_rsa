#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>
#include <cmath>
#include <iostream>
#include <vector>
#include <tf/transform_listener.h>
#include <motion_node/Route.h>
#include <sensor_msgs/LaserScan.h>
#include <unistd.h>


#define JUMP_SIZE   5
#define THRESHOLD   2

#define LINEAR_THRESHOLD    0.2
#define ANGULAR_THRESHOLD   0.2

#define MIN_ANGLE   100
#define MAX_ANGLE   600

#define INITIAL_POINT 0

#define BAD_POINT 9001

#define LEFT                1
#define RIGHT               -1
#define INIT_MIN_DIST       5
#define AVOIDANCE_ANGLE     1.5
#define AVOIDANCE_TIME      1
#define AVOIDANCE_DIST      0.2
#define MIN_DIST_THRESHOLD  0.15
#define NORM_DRIVE_SPEED 	0.3
#define NORM_TURN_SPEED		0.5

class BaseDriver
{
    ros::NodeHandle nh_;
    ros::Subscriber pos_sub;
    ros::Subscriber tf_sub;
    ros::Subscriber goal_sub;
    ros::Subscriber laser_sub;
    ros::Publisher motion;

    // Check variable so that the AI and the base driver are not
    // both publishing to the base at the same time
    bool moving;

public:
    tf::TransformListener tf_listener;

    BaseDriver(ros::NodeHandle n) {
        nh_ = n;
        motion = nh_.advertise<geometry_msgs::Twist>("cmd_vel_mux/input/navi", 1000);
        goal_sub = nh_.subscribe("/goal", 10, &BaseDriver::UpdatePosition, this);
        laser_sub = nh_.subscribe("/scan", 1, &BaseDriver::laserCallback, this);

        //set current position
        curPos.x = INITIAL_POINT;
        curPos.y = INITIAL_POINT;

        //STARTER POINT - DO NOT DELETE OR IT BORKS OUT
        geometry_msgs::Point p;
        p.x = INITIAL_POINT;
        p.y = INITIAL_POINT;
        curPath.push_back(p);
        curGoal = p;
        curWaypoint = 0;

        std::cout << "Initialised" << std::endl;
        moving = false;
    }

    ~BaseDriver() {
    }

    /*
     * Callback function for the laser, for proximity detection and obstacle avoidance
     */
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan) {

        // If the AI hasn't told us to move, don't
        if (moving == false) {
            return;
        }

        //std::cout << "Avoiding wall" << std::endl;

        // Find the smallest distance in the range that we check
        float minDist = INIT_MIN_DIST;
        int angle = 0;
        for (int i = MIN_ANGLE; i < MAX_ANGLE; i ++) {
            if (std::isfinite(scan->ranges[i]) && minDist > scan->ranges[i]) {
                minDist = scan->ranges[i];
                angle = i;
            }
        }

        // If there is nothing too close, stop doing this
        if (minDist > MIN_DIST_THRESHOLD)
            return;

        // Move back a bit
        geometry_msgs::Twist vel_msg;
        vel_msg.linear.x = -AVOIDANCE_DIST*0.6;
        motion.publish(vel_msg);
        ros::Duration(AVOIDANCE_TIME).sleep();
        vel_msg.linear.x = 0;
        motion.publish(vel_msg);

        // Determine which way we need to turn, and rotate pi/2 radians that way
        int direction = angle < (MIN_ANGLE + MAX_ANGLE)/2? LEFT : RIGHT;
        float curAngle = 0;
        vel_msg.angular.z = AVOIDANCE_ANGLE*direction;
        motion.publish(vel_msg);
        ros::Duration(AVOIDANCE_TIME).sleep();
        vel_msg.angular.z = 0;
        motion.publish(vel_msg);

        // Move forward 20cm
        vel_msg.linear.x = AVOIDANCE_DIST;
        motion.publish(vel_msg);
        ros::Duration(AVOIDANCE_TIME).sleep();
        vel_msg.linear.x = 0;
        motion.publish(vel_msg);
    }

    /*
     *  Callback function for when route messages are received
	   * UPDATE: WE NOW TAKE IN A SINGLE POINT
     */
	void UpdatePosition(const geometry_msgs::Point::ConstPtr& p) {
        if(curGoal.x == BAD_POINT){
            //  no solution found by the A* publishing stuff
            moving = false;
            return;
        }
		    curGoal.x = p->x;
		    curGoal.y = p->y;
        moving = true;
        //std::cout << "We can now move" << std::endl;
    }

    /*
     *  Actually drive the robot to position indicated by relGoal
     */
    void moveToPosition() {
        // If the AI hasn't told us to move, don't
        if (moving == false) {
            return;
        }

        // get our current position in the map
        geometry_msgs::Point relGoal;
        relGoal = transformMapToBase(curGoal);
        std::cout << "Relative goal is "<< relGoal.x << ", " << relGoal.y << std::endl;
        std::cout << "Absolute goal is "<< curGoal.x << ", " << curGoal.y << std::endl;


        // Are we at the goal? If so, pick the next point.
        // If not, continue
        if (dist(relGoal, curPos) < LINEAR_THRESHOLD) {
			         return;
        }

        geometry_msgs::Twist vel_msg;
        vel_msg.angular.z = 0;
        vel_msg.linear.x = 0;
        // If we are not aligned angularly,
        // rotate the bot through the smallest angle
        float theta = atan2(relGoal.y, relGoal.x);
        int parity = theta > 0 ? 1 : -1;

        // Use the perpendicular distance and the side the point is on
        // to rotate at an appropriate speed in the right direction
        if (theta*parity > ANGULAR_THRESHOLD) {
            int twist = parity*theta > 0.5 ? theta : 0.5;
            //std::cout << "Setting angular to " << theta << std::endl;
            vel_msg.angular.z = NORM_TURN_SPEED*parity;
        } else {

            // If we are not on the spot laterally,
            // grind dem gears and spin dem wheels
            if (dist(relGoal, curPos) > LINEAR_THRESHOLD) {
                //std::cout << "Setting linear to 0.7" << std::endl;
                vel_msg.linear.x = NORM_DRIVE_SPEED;
            }
        }

        motion.publish(vel_msg);
    }

private:
    std::vector<geometry_msgs::Point> curPath;
    geometry_msgs::Point curPos;
    int curWaypoint;
    geometry_msgs::Point curGoal;
    geometry_msgs::PoseStamped tfGoal;
    tf::StampedTransform baseTf;


    // return the next point in the path to navigate to, or
    // the current point if we have exhausted the list
    geometry_msgs::Point nextPoint() {
        curWaypoint ++;
        //std::cout << "Points list length: " << curPath.size() << "\n";
        return curPath.size() > curWaypoint ? curPath[curWaypoint] : curPath[curPath.size() - 1];
    }


    // transform a map-relative point into a robot-relative point
    // Danger - your brain may melt when you read this
    // C obfustication challenge entrant 2015
    geometry_msgs::Point transformMapToBase(geometry_msgs::Point p) {
        geometry_msgs::PointStamped robot_point;
        geometry_msgs::PointStamped map_point_stamped;
        robot_point.header.frame_id = "/map";

        // get latest transform
        robot_point.header.stamp = ros::Time::now();

        // set the point coordinates
        robot_point.point.x = p.x;
        robot_point.point.y = p.y;
        robot_point.point.z = 0;

        // get the transform
        try {
            tf_listener.waitForTransform("/map", "/base_link", robot_point.header.stamp, ros::Duration(1.5));
            tf_listener.transformPoint("/base_link", robot_point, map_point_stamped);
        }
        catch (tf::TransformException& ex) {
            ROS_ERROR("%s\n", ex.what());
        }

        // Changed StampedPoint to just a normal Point
        geometry_msgs::Point map_point;
        map_point.x = map_point_stamped.point.x;
        map_point.y = map_point_stamped.point.y;

        return map_point;
    }


    // Find the Euclidean distance between two points
    float dist(geometry_msgs::Point a, geometry_msgs::Point b) {
        return sqrt(pow(a.x-b.x,2) + pow(a.y-b.y,2));
    }
};

//*********************************//
// Loop on moving to the next position
int main(int argc, char** argv) {
    ros::init(argc, argv, "motion_node");
        ROS_ERROR("In main");
    ros::NodeHandle n;
    BaseDriver bd(n);

    ros::Rate loop_rate(10);

    while (n.ok()) {
        //go to next position
        bd.moveToPosition();

        // Spin
        ros::spinOnce();
    }
}
