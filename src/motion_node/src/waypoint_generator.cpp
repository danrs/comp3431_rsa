#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>
#include <iostream>
#include <vector>
#include <motion_node/Route.h>
#include <unistd.h>
#include <stdio.h>

/*
 * Make way while the sun shines
 *
 * No, seriously. This class reads waypoints from the user and
 * publishes them.
 */
class WaypointGenerator{
    ros::NodeHandle nh_;
    ros::Publisher show_the_way_;


public:
    WaypointGenerator(ros::NodeHandle n) {
        nh_ = n;
        show_the_way_ = nh_.advertise<motion_node::Route>("/goal_path", 1000);
        std::cout << "Initialised" << std::endl;
    }


    ~WaypointGenerator() {
    }


    /*
     * Clear the current set of points, then read a fresh set from the user
     */
    void readPoints() {
        int numPoints;
        geometry_msgs::Point p;

        //clear points to start with
        waypoints.clear();

        //Find out how many points we are making
        std::cout << "How many points do you want to make?" << "\n";
        std::cin >> numPoints;
        if(std::cin.fail()){
            //Gotta clear the input buffer, what a trek...
            std::cin.clear();
            std::cin.ignore(100, '\n'); //ignore next 100 chars or until new line
        }

        if(numPoints > 20){
            std::cout << "This is gonna take ages...\n";
        }

        std::cout << "Enter the coordinates for your points:" << "\n";
        for(int i=0; i<numPoints; i++){
            //read x coord
            std::cout << "x" << i << ": ";
            std::cin >> p.x;
            if(std::cin.fail()){
                std::cin.clear();
                std::cin.ignore(100, '\n');
            }

            //read y coord
            std::cout << "y" << i << ": ";
            std::cin >> p.y;
            if(std::cin.fail()){
                std::cin.clear();
                std::cin.ignore(100, '\n');
            }
            waypoints.push_back(p);
        }

    }


    /*
     * Publish the current set of points
     */
    void publishPoints(){
        //check we actually have some points
        if(waypoints.size() < 1){
            return;
        }

        //build a message
        motion_node::Route way_msg;
        way_msg.path_length = waypoints.size();
        for(int i=0; i < waypoints.size(); ++i){
            way_msg.points.push_back(waypoints[i]);
        }

        show_the_way_.publish(way_msg);
        std::cout << "Points published\n";
    }


    /*
     * Clear the current set of points
     */
    void clearPoints(){
        waypoints.clear();
    }


private:
    std::vector<geometry_msgs::Point> waypoints;

};


int main(int argc, char** argv) {
    ros::init(argc, argv, "waypoint_generator");
    ros::NodeHandle n;
    WaypointGenerator wg(n);
    int numPoints;

    ros::Rate loop_rate(1);

    while (n.ok()) {
        //Read points from user and publish
        wg.readPoints();
        wg.publishPoints();
        wg.clearPoints();

        // Spin, we don't really need this
        ros::spinOnce();

        // Sleep until end of specified rate
        loop_rate.sleep();
    }
}

