#ifndef _ASTAR2_H_
#define _ASTAR2_H_

#include <cstdlib>
#include <cmath>
#include <queue>
#include <cstdint>
#include <iostream>
#include <geometry_msgs/Point.h>

void astar_2(std::queue<geometry_msgs::Point>& solution, uint16_t start_x, uint16_t start_y, uint16_t end_x, uint16_t end_y, std::vector<int8_t>& costMap);


#endif
