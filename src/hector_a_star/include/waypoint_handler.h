#ifndef _WAYPOINT_HANDLER_H_
#define _WAYPOINT_HANDLER_H_

#include <ros/ros.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>
#include <iostream>
#include <vector>
#include <unistd.h>
#include <stdio.h>

/*
* Make way while the sun shines
*
* No, seriously. This class reads waypoints from the user and
* publishes them.
*/
class WaypointHandler{
	ros::NodeHandle nh_;
	ros::Publisher show_the_way_;


public:
	WaypointHandler(ros::NodeHandle n);
	~WaypointHandler();

	void addPoint(geometry_msgs::Point p);
	void publishPoints();
	void clearPoints();


private:
	std::vector<geometry_msgs::Point> waypoints;

};

#endif
