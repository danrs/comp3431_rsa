/*
 * Struct for describing beacons on the map (absolute position)
 */
typedef struct map_beacon{
    int id;
    int x;
    int y;
} map_beacon;
