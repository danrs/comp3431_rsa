// a program to parse /map and other topics and publish useful information (hopefully)

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sstream>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
//#include <astar.h>
#include <astar_2.h>
#include <waypoint_handler.h>
#include <cstdlib>
#include <cmath>
#include <tf/transform_listener.h>

#define RESOLUTION 	0.05
#define MAP_MULTIPLIER  20

#define NO_SOLUTION 9001

/*
occ. grid:  http://docs.ros.org/api/nav_msgs/html/msg/OccupancyGrid.html
point:      http://docs.ros.org/api/geometry_msgs/html/msg/Point.html
quaternion: http://docs.ros.org/api/geometry_msgs/html/msg/Quaternion.html
*/

#define COST_STAMP_SIZE 10 
#define COST_STAMP_MAX 0  

class HCostmap{
    // place for public methods and vars
    public:
        HCostmap(ros::NodeHandle n);
        void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& h_map);
		geometry_msgs::Point transformBaseToMap(geometry_msgs::Point p);
		void endpointCallback(const geometry_msgs::Point::ConstPtr& p);

        ros::NodeHandle nodeHandler_;
        ros::Subscriber map_subscriber_;
		ros::Subscriber endpoint_subscriber_;
 		//ros::Subscriber beacon_subscriber_
		tf::TransformListener tf_listener_;
        ros::Publisher costmap_publisher_;
        ros::Publisher pos_publisher_;
        ros::Publisher orientation_publisher_;
		ros::Publisher a_star_publisher_;
		ros::Publisher next_point_publisher_;
        
	    WaypointHandler wh;

		geometry_msgs::Point curEndPoint;
        
    private:
        uint8_t cost_stamp[COST_STAMP_SIZE][COST_STAMP_SIZE];
};

HCostmap::HCostmap(ros::NodeHandle n) : wh(n) {
    nodeHandler_ = n;
	curEndPoint.x = 125; curEndPoint.y = 125;
    // subscribed topics
    map_subscriber_ = nodeHandler_.subscribe<nav_msgs::OccupancyGrid>("map", 1000, &HCostmap::mapCallback, this);
    //beacon_subscriber_ = nodeHandler_.subscribe<hugbot::beacon_msg>("/beacon", 1000, &HCostmap::beaconCallback, this);
	endpoint_subscriber_ = nodeHandler_.subscribe<geometry_msgs::Point>("/endpoint", 10, &HCostmap::endpointCallback, this); //change to /goal if using plan B


    // published topics
    costmap_publisher_ = nodeHandler_.advertise<nav_msgs::OccupancyGrid>("hector_costmap", 1000);
    pos_publisher_ = nodeHandler_.advertise<std_msgs::String>("hector_costmap_test_string", 1000);

    pos_publisher_ = nodeHandler_.advertise<geometry_msgs::Point>("hector_costmap_position", 1000);
    orientation_publisher_ = nodeHandler_.advertise< geometry_msgs::Quaternion>("hector_costmap_orientation", 1000);

	a_star_publisher_ = nodeHandler_.advertise<nav_msgs::OccupancyGrid>("a_star_path",1000);
	next_point_publisher_ = nodeHandler_.advertise<geometry_msgs::Point>("next_point",10);

    int16_t i,j;
    int centre = COST_STAMP_SIZE/2;
    for(i=0;i<COST_STAMP_SIZE;i++){
        for(j=0;j<COST_STAMP_SIZE;j++){
            cost_stamp[i][j] = fmax(0,COST_STAMP_MAX*(1 - sqrt((i-centre)*(i-centre)+(j-centre)*(j-centre))/(COST_STAMP_SIZE/2)));
			if(abs(i-centre)<5&&abs(j-centre)<5){ // MAGIC: SETS WIDTH OF WALLS make narrowerr if no solution
				cost_stamp[i][j]=99;
			}
        }
    }
}

// callback handler for getting an endpoint from the ai
void HCostmap::endpointCallback(const geometry_msgs::Point::ConstPtr& p) {
	curEndPoint.x = p->x;
	curEndPoint.y = p->y;
}

// callback handler for map messages. h_map is the hector published map grid
void HCostmap::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& h_map){

std::cout << "Mapping\n";
    nav_msgs::OccupancyGrid newMap;
	nav_msgs::OccupancyGrid aStarMap;
    newMap.data = h_map->data;
	newMap.info.width = h_map->info.width;
	newMap.info.height = h_map->info.height;
	newMap.info.resolution = h_map->info.resolution;
	newMap.info.origin = h_map->info.origin;

	aStarMap.data.assign(62500,0);
	aStarMap.info.width = h_map->info.width;
	aStarMap.info.height = h_map->info.height;
	aStarMap.info.resolution = h_map->info.resolution;
	aStarMap.info.origin = h_map->info.origin;

    // MAP OPERATIONS
    // ensure probabilities range is set
    uint32_t width = newMap.info.width;
    uint32_t height = newMap.info.height;

    int32_t i,j,x,y;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if(newMap.data[i*width+j]==100){
                for(x=i-COST_STAMP_SIZE/2;x<i+COST_STAMP_SIZE/2;x++){
					for (y = j - COST_STAMP_SIZE / 2; y < j + COST_STAMP_SIZE / 2; y++){
                        if((x>=0&&x<height)&&(y>=0&&y<width)){
                            newMap.data[x*width+y] = fmax(newMap.data[x*width+y],cost_stamp[x-i+COST_STAMP_SIZE/2][y-j+COST_STAMP_SIZE/2]);
                        }
                    }
                }
			}
			else if (newMap.data[i*width + j] == -1){
				newMap.data[i*width + j] = 0;
			}
        }
    }
	for (i = 0; i < height; i++){
		for (j = 0; j < width; j++){
			if (newMap.data[i*width + j] == -1){
				newMap.data[i*width + j] = 0;
			} else if (newMap.data[i*width + j] >=99){
				newMap.data[i*width + j] = -1;
			}
		}
	}


    // this will print the cost map
    //printMap(newMap.data);

	//Map searchMap(newMap.data);

	// publishes the costmap - for rviz visualisation
    costmap_publisher_.publish(newMap);
	std::queue<geometry_msgs::Point> solutionTest;
	// this a * works! yay, testing in real time to be done
	
	
	//geometry_msgs::Point org = getRobotMapLocation();
	//std::cout << org.x << "  " << org.y << std::endl;
	//checking where the robot is
    geometry_msgs::Point base_point;
    geometry_msgs::Point map_point;
    base_point.x = 0;
    base_point.y = 0;
    base_point.z = 0;
    map_point = transformBaseToMap(base_point);
	printf("Map point = (%f, %f)\n",map_point.x,map_point.y);
	printf("Origin position = (%f, %f)\n",newMap.info.origin.position.x,newMap.info.origin.position.y);
	printf("Calculated cost map index = (%d, %d)\n", (uint16_t)((map_point.x-newMap.info.origin.position.x)*MAP_MULTIPLIER),(uint16_t)((map_point.y-newMap.info.origin.position.y)*MAP_MULTIPLIER));

	astar_2(solutionTest, (uint16_t)((map_point.x-newMap.info.origin.position.x)*MAP_MULTIPLIER), (uint16_t)((map_point.x-newMap.info.origin.position.x)*MAP_MULTIPLIER), curEndPoint.x , curEndPoint.y, newMap.data);
	geometry_msgs::Point next_point;	
	if(solutionTest.empty()){
		next_point.x = NO_SOLUTION;
		next_point.y = NO_SOLUTION;
		next_point_publisher_.publish(next_point);
		printf("Publishing %g %g\n", next_point.x,next_point.y);
	} else if(solutionTest.size()<6){
		next_point.x = (double)solutionTest.back().x/MAP_MULTIPLIER+newMap.info.origin.position.x;
		next_point.y = (double)solutionTest.back().y/MAP_MULTIPLIER+newMap.info.origin.position.y;
		next_point_publisher_.publish(next_point);
		printf("Publishing %g %g\n", next_point.x,next_point.y);
	} else {
		for(i=0;i<5;i++){
			aStarMap.data[solutionTest.front().y*width+solutionTest.front().x] = 100;
			solutionTest.pop();
		}
		next_point.x = (double)solutionTest.front().x/MAP_MULTIPLIER+newMap.info.origin.position.x;
		next_point.y = (double)solutionTest.front().y/MAP_MULTIPLIER+newMap.info.origin.position.y;
		next_point_publisher_.publish(next_point);
		printf("Publishing %g %g\n", next_point.x,next_point.y);
	}
	while(!solutionTest.empty()){
		//std::cout << "(" << solutionTest.front().x << ", " << solutionTest.front().y << "):" << solutionTest.front().y*width+solutionTest.front().x << std::endl;
		aStarMap.data[solutionTest.front().y*width+solutionTest.front().x] = 100;
		solutionTest.pop();
	}
	a_star_publisher_.publish(aStarMap);

	
}

/*
 * Beacon callback
 * Transforms the beacon to map coordinates and stores it
 * If a duplicate beacon is spotted, the old one is replaced
 */
/*void HCostmap::beaconCallback(const hugbot::beacon_msg::ConstPtr& beacon){
    map_beacon b;
    geometry_msgs::Point base_point;
    geometry_msgs::Point map_point;

    //some trig
    base_point.x = beacon->range*cos(beacon->angle);
    base_point.y = beacon->range*sin(beacon->angle);
    base_point.z = 0;

    map_point = transformBaseToMap(base_point);

    b.id = beacon->id;
    b.x = map_point.x;
    b.y = map_point.y;

    //insert if not present, replace if present
    if(known_beacons.count(b.id)){
        known_beacons.erase(b.id);
    }
    known_beacons.insert(std::map<int, map_beacon>::value_type(b.id, b));
    std::cout << "Beacon saved: id=" << b.id << " x=" << b.x << " y=" << b.y << "\n";

}
*/

/*
 * Transform a base-relative point into a map-relative point
 */
geometry_msgs::Point HCostmap::transformBaseToMap(geometry_msgs::Point p) {
    geometry_msgs::PointStamped robot_point;
    geometry_msgs::PointStamped map_point_stamped;
    robot_point.header.frame_id = "/base_link";

    // get latest transform
    robot_point.header.stamp = ros::Time::now();

    // set the point coordinates
    robot_point.point.x = p.x;
    robot_point.point.y = p.y;
    robot_point.point.z = 0;

    // get the transform
    try {
        tf_listener_.waitForTransform("/base_link", "/map", robot_point.header.stamp, ros::Duration(1.5));
        tf_listener_.transformPoint("/map", robot_point, map_point_stamped);
    }
    catch (tf::TransformException& ex) {
        ROS_ERROR("%s\n", ex.what());
    }

    // Changed StampedPoint to just a normal Point
    geometry_msgs::Point map_point;
    map_point.x = map_point_stamped.point.x;
    map_point.y = map_point_stamped.point.y;

    return map_point;
}

/*
 * Gets a list of all seen beacons
 */
/*std::vector<map_beacon> HCostmap::getBeaconPoints() {
    std::vector<map_beacon> list;
    map_beacon p;
    for(std::map<int, map_beacon>::iterator it = known_beacons.begin(); it != known_beacons.end(); ++it){
        p.x = it->second.x;
        p.y = it->second.y;
        p.id = it->second.id;
        list.push_back(p);
    }
    return list;

}*/



int main(int argc, char* argv[]){
    ros::init(argc, argv, "hector_costmap");
    ros::NodeHandle n;
std::cout << "Starting hector\n";
	
    // hector_costmap object
    HCostmap cmap(n);
    // 10hz update
    ros::Rate loop_rate(10);

    // spin the rosbot :D
    ros::spin();
    return EXIT_SUCCESS;
}






