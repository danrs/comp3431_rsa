#include "waypoint_handler.h"
#include <motion_node/Route.h>

WaypointHandler::WaypointHandler(ros::NodeHandle n) {
	nh_ = n;
	show_the_way_ = nh_.advertise<motion_node::Route>("/goal_path", 1000);
	std::cout << "Initialised" << std::endl;
}


WaypointHandler::~WaypointHandler() {
}


/*
* Clear the current set of points, then read a fresh set from the user
*/
void WaypointHandler::addPoint(geometry_msgs::Point p) {
	waypoints.push_back(p);
}


/*
* Publish the current set of points
*/
void WaypointHandler::publishPoints(){
	//check we actually have some points
	if (waypoints.size() < 1){
		return;
	}

	//build a message
	motion_node::Route way_msg;
	way_msg.path_length = waypoints.size();
	for (int i = 0; i < waypoints.size(); ++i){
		way_msg.points.push_back(waypoints[i]);
	}

	show_the_way_.publish(way_msg);
	std::cout << "Points published\n";
}


/*
* Clear the current set of points
*/
void WaypointHandler::clearPoints(){
	waypoints.clear();
}

