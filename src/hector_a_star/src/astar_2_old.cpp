#include "astar_2.h"

#define MAP_SIZE 100

class Member {
public:
	Member(int8_t x_, int8_t y_, double fScore_) {
		x = x_;
		y = y_;
		fScore = fScore_;
	}

	//Member(){}
	bool operator<(const Member& rhs) const{
		return fScore > rhs.fScore;
	}
	int8_t x;
	int8_t y;
	double fScore;
private:

};

double CellsCostToGo[MAP_SIZE][MAP_SIZE];
uint16_t MyBestParent[MAP_SIZE][MAP_SIZE];

void analyzeChild(Member *child, Member *parent, const std::vector<int8_t>& costMap, double *curF, int start_x, int start_y, std::priority_queue<Member>& thisQueue);
void getBestPath(int start_x, int start_y, const std::queue<std::vector<int8_t>>& solution);
	

void astar_2(int * solution, int start_x, int start_y, int end_x, int end_y, std::vector<int8_t>& costMap){
	int i,j,count;
	double costOfThisCell, costOfMove, costOfMoveDiag;
	for(i=0;i<100;i++){
		for(j=0;j<100;j++){
			CellsCostToGo[i][j] = 10000;
		}
	}
	CellsCostToGo[end_y][end_x] = 0;
	for(i=0;i<100;i++){
		for(j=0;j<100;j++){
			MyBestParent[i][j] = 10000;
		}
	}
	double currentFscore = sqrt((end_x-start_x)*(end_x-start_x)+(end_y-start_y)*(end_y-start_y));
	std::cout << "Starting score " << currentFscore << std::endl;
	std::priority_queue<Member> frontier;
	std::queue<std::vector<int8_t>> path;
	Member destination(end_x,end_y,0);
	frontier.push(destination);
	count = 0;
	while(true){
		if(frontier.size()==0){
			std::cout << "Empty queue" << std::endl;
			break;
		}
		//std::cout << "Iteration" << count++ << std::endl;
		Member Parent;
		Parent = frontier.top();
		if(Parent.x == start_x && Parent.y == start_y){
			std::cout << "Solution found" << std::endl;
			break;
		}
		costOfThisCell = CellsCostToGo[Parent.y][Parent.x];
		costOfMove = costOfThisCell+1;
		costOfMoveDiag = costOfThisCell+sqrt((double)2);
		for(i=-1;i<=1;i++){
			for(j=-1;j<=1;j++){
				if((Parent.x+j>=0)&&(Parent.y+i>=0)&&(Parent.x+j<MAP_SIZE)&&(Parent.y+i<MAP_SIZE)){
					if(!((Parent.x+j==0)&&(Parent.y+i==0))){
						if(i==0||j==0){
							Member Child(Parent.x+j,Parent.y+i,costOfMove);
							analyzeChild(&Child, &Parent, costMap, &currentFscore, start_x, start_y, frontier);
						} else {
							Member Child(Parent.x+j,Parent.y+i,costOfMoveDiag);
							analyzeChild(&Child, &Parent, costMap, &currentFscore, start_x, start_y, frontier);
						}
					}
				}
			}
		}
		frontier.pop();	
	}
	std::cout << "Get to here" << std::endl;
	getBestPath(start_x, start_y, path);

}

void getBestPath(int start_x, int start_y, const std::queue<std::vector<int8_t>>& solution){
	//std::vector<int8_t> newPath;
	uint16_t parent_i = MyBestParent[start_y][start_x];
	uint16_t x,y;
	for(y=0;y<MAP_SIZE;y++){
		for(x=0;x<MAP_SIZE;x++){
			printf("%5d ",MyBestParent[y][x]);
		}
		std::cout << std::endl;
	}
	std::cout << std::endl << std::endl << std::endl;
	for(y=0;y<MAP_SIZE;y++){
		for(x=0;x<MAP_SIZE;x++){
			printf("%.5g ",CellsCostToGo[y][x]);
		}
		std::cout << std::endl;
	}

	if(parent_i==10000){
		std::cout << "No solution" << std::endl;
		return;
	}
	while(parent_i!=10000){
		y = parent_i/MAP_SIZE;
		x = parent_i - y*MAP_SIZE;
		std::cout << "(" << x << ", " << y << "):" << std::endl;
		parent_i = MyBestParent[y][x];
	}
	std::cout << parent_i << std::endl;
		
}


void analyzeChild(Member *child, Member *parent, const std::vector<int8_t>& costMap, double *curF, int start_x, int start_y, std::priority_queue<Member>& thisQueue){
	if(costMap[child->y*MAP_SIZE+child->x]==static_cast<int8_t>(-1)){
		//obstacle!
		return;
	}
	
	double currentChildCost = CellsCostToGo[child->y][child->x];
	if(child->fScore>=currentChildCost){
		// proposed cost is worse than previously proposed
		return;
	}

	CellsCostToGo[child->y][child->x] = child->fScore;
	MyBestParent[child->y][child->x] = parent->y*MAP_SIZE+parent->x;
	if(currentChildCost>=10000){
		*curF = child->fScore + sqrt((child->x-start_x)*(child->x-start_x) + (child->y-start_y)*(child->y-start_y));
		child->fScore = *curF;
		Member tempChild;
		tempChild.x = child->x;
		tempChild.y = child->y;
		tempChild.fScore = child->fScore;
		thisQueue.push(tempChild);
	//std::cout << "(" << (int)tempChild.x << ", " << (int)tempChild.y << "):" << std::endl;
	}
}
