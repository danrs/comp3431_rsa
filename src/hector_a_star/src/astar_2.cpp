#include "astar_2.h"


#define MAP_SIZE 250

// Class to score location and cost of each cell
class Member {
public:
	Member(uint16_t x_, uint16_t y_, double fScore_) {
		x = x_;
		y = y_;
		fScore = fScore_;
	}

	Member(){}
    // Operator overload to invert sort of priority queue
	bool operator<(const Member& rhs) const{
		return fScore > rhs.fScore;
	}
	uint16_t x;
	uint16_t y;
	double fScore;
private:

};

// 2D array to store current cost of cells
double CellsCostToGo[MAP_SIZE][MAP_SIZE];
// 2D array to store cells' best parent cell
uint16_t MyBestParent[MAP_SIZE][MAP_SIZE];

void analyzeChild(Member *child, Member *parent, const std::vector<int8_t>& costMap, double *curF, uint16_t start_x, uint16_t start_y, std::priority_queue<Member>& thisQueue);
void getBestPath(uint16_t start_x, uint16_t start_y, std::queue<geometry_msgs::Point>& solution);


void astar_2(std::queue<geometry_msgs::Point>& solution, uint16_t start_x, uint16_t start_y, uint16_t end_x, uint16_t end_y, std::vector<int8_t>& costMap){
	int i,j,count;
	// Initialize cell cost and parent arrays
	double costOfThisCell, costOfMove, costOfMoveDiag;
	for(i=0;i<MAP_SIZE;i++){
		for(j=0;j<MAP_SIZE;j++){
			CellsCostToGo[i][j] = 10000;
		}
	}
	CellsCostToGo[end_y][end_x] = 0;
	for(i=0;i<MAP_SIZE;i++){
		for(j=0;j<MAP_SIZE;j++){
			MyBestParent[i][j] = 10000;
		}
	}

	// Initialize priority queue and push first item
	double currentFscore = sqrt((end_x-start_x)*(end_x-start_x)+(end_y-start_y)*(end_y-start_y));
	std::cout << "Starting score " << currentFscore << std::endl;
	std::priority_queue<Member> frontier;
	Member destination(end_x,end_y,0);
	frontier.push(destination);

	while(true){
        // If queue is empty, then no solution is possible
		if(frontier.size()==0){
			std::cout << "Empty queue" << std::endl;
			break;
		}
		Member Parent;
		Parent = frontier.top();
		if(Parent.x == start_x && Parent.y == start_y){
			std::cout << "Solution found" << std::endl;
			break;
		}
        // Define costs of motion to child cells
		costOfThisCell = CellsCostToGo[Parent.y][Parent.x];
		costOfMove = costOfThisCell+1;
		costOfMoveDiag = costOfThisCell+sqrt((double)2);

		// Analyze possible child cells around parent cell
		for(i=-1;i<=1;i++){
			for(j=-1;j<=1;j++){
                // this makes sure that the current cell is in the map
				if((Parent.x+j>=0)&&(Parent.y+i>=0)&&(Parent.x+j<MAP_SIZE)&&(Parent.y+i<MAP_SIZE)){

                    if(!((Parent.x+j==0)&&(Parent.y+i==0))){
                        if(i==0||j==0){
							Member Child(Parent.x+j,Parent.y+i,costOfMove);
							analyzeChild(&Child, &Parent, costMap, &currentFscore, start_x, start_y, frontier);
						} else {
							Member Child(Parent.x+j,Parent.y+i,costOfMoveDiag);
							analyzeChild(&Child, &Parent, costMap, &currentFscore, start_x, start_y, frontier);
						}
					}
				}
			}
		}
		// Clear the expanded cell from the queue
		frontier.pop();
	}

	getBestPath(start_x, start_y, solution);

}

// Function to follow trail of cells along solution path (if solution exists) and add to solution path queue
void getBestPath(uint16_t start_x, uint16_t start_y, std::queue<geometry_msgs::Point>& solution){
	//std::vector<int8_t> newPath;
	uint16_t parent_i = MyBestParent[start_y][start_x];
	geometry_msgs::Point nextNode;
	int x,y;

	while(parent_i!=10000){
		nextNode.y = parent_i/MAP_SIZE;
		nextNode.x = parent_i - nextNode.y*MAP_SIZE;
		solution.push(nextNode);
		parent_i = MyBestParent[(uint16_t)nextNode.y][(uint16_t)nextNode.x];
	}

}

// Analyzes child cell of parent cell and determines if the cost of this child is lower than existing explorations of that child cell
void analyzeChild(Member *child, Member *parent, const std::vector<int8_t>& costMap, double *curF, uint16_t start_x, uint16_t start_y, std::priority_queue<Member>& thisQueue){
	if(costMap[child->y*MAP_SIZE+child->x]==-1){
		//obstacle!
		//std::cout << "Hit wall." <<std::endl;
		return;
	}
	double heuristic = sqrt((child->x-start_x)*(child->x-start_x) + (child->y-start_y)*(child->y-start_y));
	// comparison of current explored cost with new proposed cost
	// Note: forgot to add heuristic to check in testing.
	double currentChildCost = CellsCostToGo[child->y][child->x] + heuristic;
	//std::cout << child->fScore << std::endl;
	if(child->fScore>=currentChildCost){
		// proposed cost is equal or worse than previously proposed
		return;
	}

    // Update cost of child and best parent
	CellsCostToGo[child->y][child->x] = child->fScore;
	MyBestParent[child->y][child->x] = parent->y*MAP_SIZE+parent->x;

	// If child had yet to be explored, push to queue!
	if(currentChildCost-heuristic>=10000){
		*curF = child->fScore + heuristic;
		child->fScore = *curF;
		Member tempChild;
		tempChild.x = child->x;
		tempChild.y = child->y;
		tempChild.fScore = child->fScore;
		thisQueue.push(tempChild);
	//std::cout << "(" << (int)tempChild.x << ", " << (int)tempChild.y << "):" << std::endl;
	}
}
