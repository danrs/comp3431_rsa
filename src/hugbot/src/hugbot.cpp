#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include <ros/console.h>

geometry_msgs::Twist msg;
int sentAngle = 0;

void min_distanceCallback(const sensor_msgs::LaserScan::ConstPtr& scan) {
	float range = scan->range_min;
	int vars = ((-scan->angle_min)+scan->angle_max)/scan->angle_increment;

	int i = 0;
	int angle = 0;
	float min = 0.4;
	float glob_min = 0.2;
	float max = 1.0;
	while(i < vars) {
		if (min > scan->ranges[i]) {
			min = scan->ranges[i];
		}
		i ++;
	}
	if (scan->ranges[vars/2] > (max+0.1)){
		msg.angular.z = 0.3;//angle;
		msg.linear.x = 0.0;//forward speed
	} else if (scan->ranges[vars/2] < glob_min) {
		msg.angular.z = 0;
		msg.linear.x = 0;
	} else {
		msg.angular.z = 0.0;//angle;
		msg.linear.x = 0.3;//forward speed
	}

	sentAngle = 0;
}



int main(int argc, char** argv) {

	ros::init(argc, argv, "min_distance");

	ros::NodeHandle n;


	ros::Subscriber sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1000, &min_distanceCallback);
	ros::Publisher cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1000);
		
	while (1) {
		if (sentAngle == 0) {
			cmd_vel_pub.publish(msg);
			sentAngle = 1;
		}

		ros::spinOnce();
	}
	
	return 0;
}


