#include "ros/ros.h"
#include "std_msgs/String.h"
#include <sstream>
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"

class HugBotDriver
{
public:
    HugBotDriver(ros::NodeHandle n);
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
private:
    ros::NodeHandle nh_;
    ros::Publisher pub_;
    ros::Subscriber sub_;
};

HugBotDriver::HugBotDriver(ros::NodeHandle n)
{
	nh_ = n;
	sub_ = nh_.subscribe<sensor_msgs::LaserScan>("/scan", 1000, 
		&HugBotDriver::laserCallback, this);
	pub_ = nh_.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1000);
}

void HugBotDriver::laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
{
	
	float range = scan->range_min;
        int vars = ((-scan->angle_min)+scan->angle_max)/scan->angle_increment;
	geometry_msgs::Twist msg;
        int i = 0;
        int angle = 0;
        float min = 0.4;
        float glob_min = 0.2;
        float max = 1.0;
        while(i < vars) {
                if (min > scan->ranges[i]) {
                        min = scan->ranges[i];
                }
                i ++;
        }
        if (scan->ranges[vars/2] > (max+0.1)){
                msg.angular.z = 0.3;//angle;
                msg.linear.x = 0.0;//forward speed
        } else if (scan->ranges[vars/2] < glob_min) {
                msg.angular.z = 0;
                msg.linear.x = 0;
        } else {
                msg.angular.z = 0.0;//angle;
                msg.linear.x = 0.3;//forward speed
        }

	pub_.publish(msg);
	ros::spinOnce();
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "HugBotDriver");
	ros::NodeHandle n;
	
	HugBotDriver bot(n);
	
        ros::spin();
    return 0;
}
