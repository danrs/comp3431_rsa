import cv2
import numpy as np
import sys


# load a colour image and get dimensions
name = sys.argv[1]
img = cv2.imread(name + '.jpg',cv2.IMREAD_COLOR)
height, width, depth = img.shape
img2 = cv2.resize(img, (640,480))

# convert to HSV
img_hsv = cv2.cvtColor(img2, cv2.COLOR_BGR2HSV)

PINK_MIN = np.array([130, 120, 50],np.uint8)
PINK_MAX = np.array([170, 255, 255],np.uint8)

GREEN_MIN = np.array([50, 150, 50],np.uint8)
GREEN_MAX = np.array([90, 255, 255],np.uint8)

BLUE_MIN = np.array([100, 150, 50],np.uint8)
BLUE_MAX = np.array([120, 255, 255],np.uint8)

YELLOW_MIN = np.array([20, 120, 50],np.uint8)
YELLOW_MAX = np.array([40, 255, 255],np.uint8)

# blur with a median filter with an aperture of 5
img_blur = cv2.medianBlur(img_hsv, 5)
# Gaussian blur, kernel size 15, sigma = 31
img_blur = cv2.GaussianBlur(img_hsv, (15,15), 15, img_blur, 15)

# Erosion would get rid of small spotty noise,
# and dilation should improve holes of high intensity
# that are missing from the beacons

frame_threshed = cv2.inRange(img_blur, GREEN_MIN, GREEN_MAX)
cv2.imshow('Threshold', frame_threshed)
#cv2.imshow('HSV', img_hsv)
#cv2.imshow('BGR', img2)

key = cv2.waitKey(0) & 0xFF

if key == 27:
	cv2.destroyAllWindows()
elif key == ord('s'):
	cv2.imwrite('test.jpg',img)
	cv2.destroyAllWindows()
