#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <vector>
#include <queue>
#include "sensor_msgs/LaserScan.h"
#include <hugbot/beacon_msg.h>
#include <Beacon.h>
#include <tf/transform_listener.h> //TODO remove listener from here if not needed?

#define X_THRESHOLD	            15
#define Y_THRESHOLD	            15
#define SEG_THRESHOLD           5
#define LONG_DIST_THRESHOLD     20
#define MIN_WIDTH	              15
#define MAX_WIDTH	              50
#define MIN_HEIGHT	            10
#define MAX_HEIGHT	            70
#define MIN_HIST_PERCENTAGE     0.4

#define CM_TO_M 0.01

#define MIN_BLOBS	    1

#define MAX_STORED_SCANS 20
#define MAX_STORED_TRANSFORMS MAX_STORED_SCANS

//Kinect camera fields of view (radians)
#define COLOR_FOV_H 1.0821
#define COLOR_FOV_V 0.8482
#define DEPTH_FOV_H 1.0210
#define DEPTH_FOV_V 0.7959

//Kinect color camera resolution (before tampering)
#define COLOR_PIXELS_X 640
#define COLOR_PIXELS_Y 480 //this is cut to 240 during processing
#define FINAL_PIXELS_Y 240

// Beacon colors
//#define PINK        1
//#define BLUE        2
//#define GREEN       3
//#define YELLOW      4
//#define NUM_COLOURS 4

/*
 * Beacon IDs
 * First digit is top color, second is bottom color
 * ID = (top_color*10) + bottom_color
 */
//#define PINK_YELLOW 14
//#define YELLOW_PINK 41
//#define PINK_GREEN  13
//#define GREEN_PINK  31
//#define PINK_BLUE   12
//#define BLUE_PINK   21

#define UNDETERMINED_BLOB   -1

// constants for the colour masks
const cv::Scalar PINK_MIN = cv::Scalar(130, 120, 50);
const cv::Scalar PINK_MAX = cv::Scalar(180, 255, 255);

const cv::Scalar BLUE_MIN = cv::Scalar(95, 150, 80);
const cv::Scalar BLUE_MAX = cv::Scalar(120, 255, 255);

const cv::Scalar YELLOW_MIN = cv::Scalar(20, 120, 50);
const cv::Scalar YELLOW_MAX = cv::Scalar(40, 255, 255);

const cv::Scalar GREEN_MIN = cv::Scalar(40, 80, 20);
const cv::Scalar GREEN_MAX = cv::Scalar(150, 255, 120);

// Structure for blobs of colour found in the image
typedef struct blob {
    int x;
    int y;
    int width;
    int height;
    int colour;
} blob;

// Structure for beacons found in the image
//typedef struct beacon {
    //fields to be published
//    int id;
//    ros::Time stamp;
//    float range;
//    float angle;

    //internal use fields (do not publish)
//    int x; // pixels
//    int y; // pixels from the top of the image, 0-240
//} beacon;


// For debugging purposes only
static const std::string OPENCV_WINDOW = "Image window";
static const std::string GREEN_WINDOW = "Green window";
static const std::string BLUE_WINDOW = "Blue window";
static const std::string YELLOW_WINDOW = "Yellow window";



class ImageConverter
{
    ros::NodeHandle nh_;
    image_transport::ImageTransport it_;    //something something opencv bridge
    image_transport::Subscriber image_sub_; //get images
    ros::Subscriber laser_sub_;             //get lasers
    ros::Publisher beacon_pub_;             //publish beacon messages
    tf::TransformListener tf_listener_;     //listener to get transforms

    private:

    // Hold recent scans to get the distance of the seen beacon
    std::queue<sensor_msgs::LaserScan::ConstPtr> recent_scans;

    // Find all blobs in a colour-segmented image
    std::vector<blob> getBlobs(cv::Mat image, int colour) {
        // White histogram for x
        const int cols = image.cols;
        const int rows = image.rows;
        int xHist[cols];
        int x,y;
        for (x = 0; x < cols; x ++)
            xHist[x] = 0;
        for (y = 0; y < rows; ++y) {
            for (x = 0; x < cols; ++x) {
                if (image.at<uchar>(y, x))
                    xHist[x] ++;
            }
        }

        // Loop through x histogram, find columns with highest white values
        // Use these to find the x-ranges of the blobs
        std::vector<blob> potentialBlobs;
        for (x = 0; x < cols; x ++) {
            if (xHist[x] > X_THRESHOLD) {
                int i = x;
                while (xHist[i] > X_THRESHOLD)
                    i ++;
                if (i - x > MIN_WIDTH) {
                    // We have a blob in the xHist
                    blob b;
                    b.width = i - x;
                    b.x = (i + x)/2;
                    b.y = UNDETERMINED_BLOB;
                    b.colour = colour;
                    potentialBlobs.push_back(b);
                }
                x = i;
            }
        }

        // Cut out the vertical slice that the blob is in,
        // and in each slice, do a y-histogram for the height of that blob.
        // If it's good, we add it to the list of blobs to be returned
        std::vector<blob> goodBlobs;
        for (int i = 0; i < potentialBlobs.size(); i ++) {
            // Get the image slice of the blob
            blob curBlob = potentialBlobs[i];

            // failsafe to avoid the OpenCV assert crashing
            if (curBlob.x - (curBlob.width/2.0) < 0 || curBlob.x + curBlob.width/2.0 > cols) {
              continue;
            }

            cv::Rect slice(MAX(curBlob.x - (curBlob.width/2) - 1, 0), 0, curBlob.width, rows);
            cv::Mat blobROI = image(slice);
            // With the image slice, get the y-histogram
            int yHist[blobROI.rows];
            for (y = 0; y < blobROI.rows; y ++)
                yHist[y] = 0;
            for (y = 0; y < blobROI.rows; ++y) {
                for (x = 0; x < blobROI.cols; ++x) {
                    if (blobROI.at<uchar>(y, x))
                        yHist[y] ++;
                }
            }

            // Determine if there is a blob in the y histogram
            int j = 0;
            for (y = 0; y < blobROI.rows; y ++) {
                if (yHist[y] > Y_THRESHOLD) {
                    j = y;
                    while (yHist[j] > Y_THRESHOLD)
                        j ++;
                    // There is a big enough blob in the y direction. Add it to the list
                    if (j - y > MIN_HEIGHT) {
                        curBlob.height = j - y;
                        curBlob.y = (j + y)/2;
                        goodBlobs.push_back(curBlob);
                        break;
                    }
                    y = j;
                }
            }
        }

        // Return the vector of blobs
        return goodBlobs;
    }


    public:
    ImageConverter()
        : it_(nh_)
    {
        // Subscribe to input video feed
        image_sub_ = it_.subscribe(//"/camera/rgb/image_color_throttle", 1,
                "/camera/rgb/image_color", 1,
                &ImageConverter::findBeacons, this);

        //Subscribe to lasers
        laser_sub_ = nh_.subscribe<sensor_msgs::LaserScan>("/scan", 20,
                &ImageConverter::laserCallback, this);

        //publish beacon data
        beacon_pub_ = nh_.advertise<hugbot::beacon_msg>("/beacon", 1000);

        // Commented out to run on the robots
        //cv::namedWindow(OPENCV_WINDOW);
    }

    ~ImageConverter()
    {
        //cv::destroyWindow(OPENCV_WINDOW);
    }

    /*
     * Store laser scans in a queue as we get them
     */
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan)
    {
        if(recent_scans.size() >= MAX_STORED_SCANS){
            recent_scans.pop();
        }
        recent_scans.push(scan);
        return;
    }

    /*
     * Publish the beacon message
     */
    void publishBeacon(beacon gondor){
        hugbot::beacon_msg minas_tirith;
        minas_tirith.id = gondor.id;
        minas_tirith.stamp = gondor.stamp;
        minas_tirith.range = gondor.range;
        minas_tirith.angle = gondor.angle;
        beacon_pub_.publish(minas_tirith);
    }

    /*
     * Calculate angle in kinect rgb field of view
     * 0 is in front, left is +ve and right it -ve
     *  <---------0+++++++++>
     */
    float calculateAngle(int pixels_from_left){
        float angle = (COLOR_FOV_H * pixels_from_left) / ((double) COLOR_PIXELS_X);
        //correct angle so 0 is center
        angle = (COLOR_FOV_H*0.5) - angle;
        return angle;
    }

    /*
     * Calculate range using lidar and angle of target in kinect rgb camera
     * see calculateAngle() for details on angle
     */
    float calculateRange(float angle){
        //transform angle from base frame to lidar frame
        //yeah, not sure if transforming an angle makes mathematical sense?
        //leaving it out for now

        //get data about scan
        sensor_msgs::LaserScan::ConstPtr scan = recent_scans.back();
        int scanSize = scan->ranges.size();

        //calculate offset from center of scan array
        int index = angle/(scan->angle_increment);

        //convert to a normal index
        index = (scanSize*0.5) - index;
        if(index < 0){
            index = 0;
        } else if(index > scanSize){
            index = scanSize;
        }

        return scan->ranges[index];
    }

    /*
     * Calculate range using the y coordinate of a beacon
     * y_pix is measured from 0-FINAL_PIXELS_Y, with 0 at the top of the image
     * Beacons are higher than the camera, so closer beacons will be
     * closer to the top of the image, and thus have a lower y value
     *
     * This function is accurate to +-5cm or so.
     */
    float calculateRangeUsingY(int y_pix){
        /* Formula and params created at mycurvefit.com using the following points:
         * pix cm
         * 207 166
         * 199 150
         * 181 115
         * 162 95
         * 138 77
         * 99 56
         *
         * These were measured under uncontrolled conditions in the lab.
         * If dave changes the y coordinate coming from his image, we will have to
         * recalibrate the parameters.
         */

        //Equation Parameters. DANGER: magic numbers! Need to be changed for new measurements
        double a = 51.5174;
        double b = 3.991679;
        double c = 5493.003;
        double d = 55058490;

        //Equation (symmetrical sigmoidal, whatever that is. Nonlinear.)
        float range = d + (a-d)/(1+pow(y_pix/c, b));

        //convert units
        range = range*CM_TO_M;

        return range;
    }

    /*
     * When we get an image, process it to find the beacons. Then,
     * use cached laser and transform data to determine the location
     * of the beacon. Beacon locations will be published.
     */
    void findBeacons(const sensor_msgs::ImageConstPtr& msg)
    {
        // Convert the ROS msg to an OpenCV image format
        cv_bridge::CvImagePtr cv_ptr;
        try
        {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception &e)
        {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }

        // Images are 640x480, so we cut the image vertically in half (i.e. halve the vertical axis)
        // And then convert to HSV for the colour filters
        cv::Mat img_hsv;
        cv::Rect roi(0, 0, cv_ptr->image.cols, cv_ptr->image.rows/2 + LONG_DIST_THRESHOLD);
        cv::Mat cropped = cv_ptr->image(roi);
        cv::cvtColor(cropped, img_hsv, CV_BGR2HSV);

        // Find all pink colours in this image first
        // Colour segmentation to pink, and find all blobs
        cv::Mat segment;
        cv::inRange(img_hsv, PINK_MIN, PINK_MAX, segment);
        std::vector<blob> pinkBlobs = this->getBlobs(segment, PINK);

        // Now test all the other colours to find the beacons
        int colourIDArray[] = {YELLOW, BLUE, GREEN};
        cv::Scalar colours[] = {YELLOW_MIN, YELLOW_MAX, BLUE_MIN, BLUE_MAX, GREEN_MIN, GREEN_MAX};
        std::vector<beacon> beacons;
        for (int i = 0; i < pinkBlobs.size(); i ++) {
            // Cut out the column of the image with the pink blob
            blob curBlob = pinkBlobs[i];
            int rectX = MAX(curBlob.x - curBlob.width/2 - SEG_THRESHOLD, 0);
            int rectY = MAX(curBlob.y - 1.5*curBlob.height - SEG_THRESHOLD, 0);
            int rectWidth = MIN(curBlob.width + 2*SEG_THRESHOLD, img_hsv.cols - rectX- 1);
            int rectHeight = MIN(3*curBlob.height + 2*SEG_THRESHOLD, img_hsv.rows - rectY - 1);

            // failsafe so that OpenCV's assert doesn't crash our node
            if (rectX < 0 || rectY < 0 || rectX + rectWidth >= img_hsv.cols || rectY + rectHeight >= img_hsv.rows) {
                continue;
            }

            // get the region of interest
            cv::Rect pinkBlobColumn(rectX, rectY, rectWidth, rectHeight);
            cv::Mat blobCol = img_hsv(pinkBlobColumn);

            for (int c = 0; c < NUM_COLOURS - PINK; c ++) {
                // Colour-segment the column for the specified colour
                cv::Mat segCol;
                cv::inRange(blobCol, colours[2*c], colours[2*c + 1], segCol);

                // Create x and y histograms to see if there is a blob of this colour.
                // If so, find centre and compare to the pink blob to determine which beacon
                // if not, continue
                const int cols = segCol.cols;
                const int rows = segCol.rows;
                int xHist[cols];
                int yHist[rows];
                int x,y;
                for (x = 0; x < cols; x ++)
                    xHist[x] = 0;
                for (y = 0; y < rows; y ++)
                    yHist[y] = 0;
                for (y = 0; y < rows; ++y) {
                    for (x = 0; x < cols; ++x) {
                        if (segCol.at<uchar>(y, x)) {
                            xHist[x] ++;
                            yHist[y] ++;
                        }
                    }
                }

                // If there are enough columns with enough white (>50% cols over threshold)
                // we consider this a blob in x
                int colCount = 0;
                for (x = 0; x < cols; x ++) {
                    if (xHist[x] > X_THRESHOLD) {
                        colCount ++;
                    }
                }

                // Not enough columns with white. Move on with life
                if (colCount < MIN_HIST_PERCENTAGE*cols)
                    continue;

                // Find the blob in the y direction, and compare to the pink blob
                int j = 0;
                bool foundBeacon = false;
                for (y = 0; y < rows; y ++) {
                    if (yHist[y] > Y_THRESHOLD) {
                        j = y;
                        while (yHist[j] > Y_THRESHOLD)
                            j ++;
                        if (j - y > MIN_HEIGHT) {
                            // We have a coloured blob
                            int colourHeight = (j + y)/2;
                            foundBeacon = true;
                            beacon b;
                            b.x = curBlob.x;
                            b.y = rectY + colourHeight;
                            // Check what the order of the colours is
                            // We should for robustness' sake check that the different colours are
                            // the right distance apart, but for now I am lazy
                            if (colourHeight > curBlob.y - rectY) {
                                b.id = PINK*UPPER_MULTIPLIER + colourIDArray[c];
                                b.y -= curBlob.height/2;
                            } else {
                                b.id = colourIDArray[c]*UPPER_MULTIPLIER + PINK;
                                b.y += curBlob.height/2;
                            }
                            beacons.push_back(b);
                            cv::circle(cv_ptr->image, cv::Point(b.x, b.y), 25, colours[2*c]);
                            break;
                        }
                        y += j;
                    }
                }

                // If we found a beacon, break; Else, continue
                if (foundBeacon) {
                    break;
                } else {
                    continue;
                }
            }
        }

        // Publish all the beacons
        for(std::vector<beacon>::iterator it = beacons.begin(); it != beacons.end(); ++it){
            it->angle = calculateAngle(it->x);
            //it->range = calculateRange(it->angle);
            it->range = calculateRangeUsingY(it->y);
            publishBeacon(*it);
        }

        // Update GUI Window
        //cv::imshow(OPENCV_WINDOW, cv_ptr->image);
        //cv::waitKey(3);
    }
};

int main(int argc, char** argv)
{
    ros::init(argc, argv, "image_converter");
    ImageConverter ic;
    ros::spin();
    return 0;
}
