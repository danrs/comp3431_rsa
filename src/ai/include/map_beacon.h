/*
 * Struct for describing beacons on the map (absolute position)
 */
typedef struct map_beacon{
    int id;
    float x;
    float y;
} map_beacon;
