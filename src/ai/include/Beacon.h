// Beacon colors
#define PINK        1
#define BLUE        2
#define GREEN       3
#define YELLOW      4
#define NUM_COLOURS 4

/*
 * Beacon IDs
 * First digit is top color, second is bottom color
 * ID = (top_color*10) + bottom_color
 */
#define PINK_YELLOW 14
#define YELLOW_PINK 41
#define PINK_GREEN  13
#define GREEN_PINK  31
#define PINK_BLUE   12
#define BLUE_PINK   21
#define NUM_COLOURS 4

#define FAKE_BEACON_LOC     9001

#define UPPER_MULTIPLIER  10

#define STRING_PINK   "pink"
#define STRING_YELLOW "yellow"
#define STRING_BLUE   "blue"
#define STRING_GREEN  "green"

#define FLOAT_PARAM(P)		(((P).getType() == XmlRpc::XmlRpcValue::TypeInt)?(double)(int)(P):(double)(P))



// Structure for beacons found in the image
typedef struct beacon {
    //fields to be published
    int id;
    ros::Time stamp;
    float range;    //TODO change this?
    float angle;      //TODO change this?

    //internal use fields (do not publish)
    int x; // pixels
    int y; // pixels from the top of the image, 0-240
} beacon;
