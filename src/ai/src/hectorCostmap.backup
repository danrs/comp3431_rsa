/*
occ. grid:  http://docs.ros.org/api/nav_msgs/html/msg/OccupancyGrid.html
point:      http://docs.ros.org/api/geometry_msgs/html/msg/Point.html
quaternion: http://docs.ros.org/api/geometry_msgs/html/msg/Quaternion.html
*/
#define COST_STAMP_SIZE 7
#define COST_STAMP_MAX 50

class HCostmap{
    // place for public methods and vars
    public:
        HCostmap(ros::NodeHandle n);
        void mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& h_map);
        void beaconCallback(const hugbot::beacon_msg::ConstPtr& beacon);
        geometry_msgs::Point transformBaseToMap(geometry_msgs::Point p);
        std::vector<map_beacon> getBeaconPoints();

        ros::NodeHandle nodeHandler_;
        ros::Subscriber map_subscriber_;
        ros::Subscriber beacon_subscriber_;
        tf::TransformListener tf_listener_;
        ros::Publisher testStr_publisher_;
        ros::Publisher costmap_publisher_;
        ros::Publisher pos_publisher_;
        ros::Publisher orientation_publisher_;

    private:
        uint8_t cost_stamp[COST_STAMP_SIZE][COST_STAMP_SIZE];
        std::map<int, map_beacon> known_beacons; //hash of seen beacons
};

HCostmap::HCostmap(ros::NodeHandle n){
    nodeHandler_ = n;

    // subscribed topics
    map_subscriber_ = nodeHandler_.subscribe<nav_msgs::OccupancyGrid>("map", 1000, &HCostmap::mapCallback, this);
    beacon_subscriber_ = nodeHandler_.subscribe<hugbot::beacon_msg>("/beacon", 1000, &HCostmap::beaconCallback, this);

    // published topics
    costmap_publisher_ = nodeHandler_.advertise<nav_msgs::OccupancyGrid>("hector_costmap", 1000);
    pos_publisher_ = nodeHandler_.advertise<std_msgs::String>("hector_costmap_test_string", 1000);
    pos_publisher_ = nodeHandler_.advertise<geometry_msgs::Point>("hector_costmap_position", 1000);
    orientation_publisher_ = nodeHandler_.advertise< geometry_msgs::Quaternion>("hector_costmap_orientation", 1000);

    //set up cost stamp
    int16_t i,j;
    int centre = COST_STAMP_SIZE/2;
    for(i=0;i<COST_STAMP_SIZE;i++){
        for(j=0;j<COST_STAMP_SIZE;j++){
            cost_stamp[i][j] = fmax(0,COST_STAMP_MAX*(1 - sqrt((i-centre)*(i-centre)+(j-centre)*(j-centre))/(COST_STAMP_SIZE/2)));
        }
    }
}


// callback handler for map messages. h_map is the hector published map grid
void HCostmap::mapCallback(const nav_msgs::OccupancyGrid::ConstPtr& h_map){

    nav_msgs::OccupancyGrid newMap;
    newMap.data = h_map->data;
	newMap.info.width = h_map->info.width;
	newMap.info.height = h_map->info.height;
    //newMap = ros::rosmessage(h_map);

    // MAP OPERATIONS
    // ensure probabilities range is set
    uint32_t width = newMap.info.width;
    uint32_t height = newMap.info.height;
    //int8_t costMap[height][width];
    //costMap = newMap.data;
	//memcpy(mapPtr, newMap.data, height*width);
    int32_t i,j,x,y;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if(newMap.data[i*width+j]==100){
                for(x=i-COST_STAMP_SIZE/2;x<i+COST_STAMP_SIZE/2;x++){
					for (y = j - COST_STAMP_SIZE / 2; y < j + COST_STAMP_SIZE / 2; y++){
                        if((x>=0&&x<height)&&(y>=0&&y<width)){
                            newMap.data[x*width+y] = fmax(newMap.data[x*width+y],cost_stamp[x-i+COST_STAMP_SIZE/2][y-j+COST_STAMP_SIZE/2]);
                        }
                    }
                }
			}
			else if (newMap.data[i*width + j] == -1){
				newMap.data[i*width + j] = 0;
			}
        }
    }
	for (i = 0; i < height; i++){
		for (j = 0; j < width; j++){
			if (newMap.data[i*width + j] == -1){
				newMap.data[i*width + j] = 0;
			} else if (newMap.data[i*width + j] == 100){
				newMap.data[i*width + j] = -1;
			}
		}
	}

    costmap_publisher_.publish(newMap);


    // POINT OPERATIONS
    // create point message - do any modifications here
    geometry_msgs::Point newPoint = geometry_msgs::Point(h_map->info.origin.position);
    pos_publisher_.publish(newPoint);

    // ORIENTATION OPERATIONS
    // create orientation message - do any modifications here
    geometry_msgs::Quaternion newOrientation = geometry_msgs::Quaternion(h_map->info.origin.orientation);
    orientation_publisher_.publish(newOrientation);


}

/*
 * Beacon callback
 * Transforms the beacon to map coordinates and stores it
 * If a duplicate beacon is spotted, the old one is replaced
 */
void HCostmap::beaconCallback(const hugbot::beacon_msg::ConstPtr& beacon){
    map_beacon b;
    geometry_msgs::Point base_point;
    geometry_msgs::Point map_point;

    //some trig
    base_point.x = beacon->range*cos(beacon->angle);
    base_point.y = beacon->range*sin(beacon->angle);
    base_point.z = 0;

    map_point = transformBaseToMap(base_point);

    b.id = beacon->id;
    b.x = map_point.x;
    b.y = map_point.y;

    //insert if not present, replace if present
    if(known_beacons.count(b.id)){
        known_beacons.erase(b.id);
    }
    known_beacons.insert(std::map<int, map_beacon>::value_type(b.id, b));

}


/*
 * Transform a base-relative point into a map-relative point
 */
geometry_msgs::Point HCostmap::transformBaseToMap(geometry_msgs::Point p) {
    geometry_msgs::PointStamped robot_point;
    geometry_msgs::PointStamped map_point_stamped;
    robot_point.header.frame_id = "/base_link";

    // get latest transform
    robot_point.header.stamp = ros::Time::now();

    // set the point coordinates
    robot_point.point.x = p.x;
    robot_point.point.y = p.y;
    robot_point.point.z = 0;

    // get the transform
    try {
        tf_listener_.waitForTransform("/base_link", "/map", robot_point.header.stamp, ros::Duration(1.5));
        tf_listener_.transformPoint("/map", robot_point, map_point_stamped);
    }
    catch (tf::TransformException& ex) {
        ROS_ERROR("%s\n", ex.what());
    }

    // Changed StampedPoint to just a normal Point
    geometry_msgs::Point map_point;
    map_point.x = map_point_stamped.point.x;
    map_point.y = map_point_stamped.point.y;

    return map_point;
}

/*
 * Gets a list of all seen beacons
 */
std::vector<map_beacon> HCostmap::getBeaconPoints() {
    std::vector<map_beacon> list;
    map_beacon p;
    for(std::map<int, map_beacon>::iterator it = known_beacons.begin(); it != known_beacons.end(); ++it){
        p.x = it->second.x;
        p.y = it->second.y;
        p.id = it->second.id;
        list.push_back(p);
    }
    return list;

}

