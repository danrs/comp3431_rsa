// ai class file

//#include "Beacon.h"
#include <ros/ros.h>
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include "map_beacon.h"
#include <XmlRpcException.h>

#include <std_msgs/String.h>
#include <sstream>

#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <queue>
#include <string>
#include <unistd.h>
#include <ai/beacon_msg.h>

#include <tf/transform_listener.h>


//#define EXPLORE 0
#define BEACON1_SEARCH 1
#define BEACON2_SEARCH 2
#define BEACON3_SEARCH 3
#define BEACON4_SEARCH 4
#define EXPLORATION    5
#define DONE 		   6

#define MIN_WALL_DIST   0.33
#define MAX_WALL_DIST   0.38
#define TOO_FAR_OUT     0.5
#define MIN_FRONT_DIST  0.40

#define MIN_RIGHT_ANGLE   20
#define MAX_RIGHT_ANGLE   290
#define MIN_FRONT_ANGLE   290
#define MAX_FRONT_ANGLE   450
#define INIT_MAX    5

#define RANGE_THRESHOLD     0.5

#define LOW_TURN_SPEED  0.15
#define HIGH_TURN_SPEED 0.7
#define HARDCORE_TURN   0.8
#define NORM_DRIVE_SPEED 0.15
#define LOW_DRIVE_SPEED 0.05

#define NEXT_BEACON     1

#define BAD_POINT 9001

#define NUM_BEACONS 4


// XXX XXX XXX XXX //
//*****************//
// BEACON STUFF    //

// NOTE FOR MATT
// We have this here and also in the image node file
// because despite what you told us about linking packages,
// and us doing literally that, it still complained.
// So this was the workaround, and, well, it worked.

// Beacon colors
#define PINK        1
#define BLUE        2
#define GREEN       3
#define YELLOW      4
#define NUM_COLOURS 4

/*
 * Beacon IDs
 * First digit is top color, second is bottom color
 * ID = (top_color*10) + bottom_color
 */
#define PINK_YELLOW 14
#define YELLOW_PINK 41
#define PINK_GREEN  13
#define GREEN_PINK  31
#define PINK_BLUE   12
#define BLUE_PINK   21
#define NUM_COLOURS 4

#define FAKE_BEACON_LOC     9001

#define UPPER_MULTIPLIER  10

#define STRING_PINK   "pink"
#define STRING_YELLOW "yellow"
#define STRING_BLUE   "blue"
#define STRING_GREEN  "green"

#define FLOAT_PARAM(P)		(((P).getType() == XmlRpc::XmlRpcValue::TypeInt)?(double)(int)(P):(double)(P))


// Structure for beacons found in the image
typedef struct beacon {
    //fields to be published
    int id;
    ros::Time stamp;
    float range;    //TODO change this?
    float angle;      //TODO change this?

    //internal use fields (do not publish)
    int x; // pixels
    int y; // pixels from the top of the image, 0-240
} beacon;


// END BEACON STUFF //
//******************//
// XXX XXX XXX XXX //




// Ai class
class Ai{

public:

    Ai(ros::NodeHandle n);

	ros::Publisher endpoint_pub;
	ros::Publisher move_pub;
	ros::Subscriber nextpoint_sub;
	ros::Publisher base_pub;

    void laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan);
    void beaconCallback(const ai::beacon_msg::ConstPtr& beaconRef);

	void nextpointCallback(const geometry_msgs::Point::ConstPtr& p);
    geometry_msgs::Point transformBaseToMap(geometry_msgs::Point p);
    void visitedBeacon(void);
    int getNextBeacon(void);

    std::map<int, int> beaconVisitOrder;

	int getState(void);
	void setState(int state);
	void seenBeacon(int id);
	int getNumBeaconsSeen(void);

  // Array to hold whether or not we have seen a particular beacon.
  // It was a quick workaround
	int beaconsSeen[42];

	std::map<int, map_beacon> beacon_list;

private:

    ros::NodeHandle nh_;

    ros::Subscriber laser_sub;
    ros::Subscriber beacon_sub;

    tf::TransformListener tf_listener_;


    // Note: the numBeaconsSeen variable only cares about
    // the beacons we need to visit. So it only icnrements when
    // we see  a beacon we need to visit and haven't already seen it
    int ai_state;
    int numBeaconsSeen;
    int nextBeaconToVisit;
};

Ai::Ai(ros::NodeHandle n) {
    nh_ = n;

	int i;
	for(i=0;i<42;i++){
		beaconsSeen[i] = 0;
	}

    ai_state = EXPLORATION;

    // Set up subscribers and publishers
    move_pub = nh_.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 1000);
    laser_sub = nh_.subscribe<sensor_msgs::LaserScan>("/scan", 1000, &Ai::laserCallback, this);
    beacon_sub = nh_.subscribe<ai::beacon_msg>("/beacon", 1, &Ai::beaconCallback, this);
	endpoint_pub = nh_.advertise<geometry_msgs::Point>("/endpoint", 10);

	nextpoint_sub = nh_.subscribe<geometry_msgs::Point>("next_point",10, &Ai::nextpointCallback, this);
	base_pub = nh_.advertise<geometry_msgs::Point>("/goal", 10);



    numBeaconsSeen = 0;
    nextBeaconToVisit = 1;
	ai_state = EXPLORATION;
}

int Ai::getState(void) {
	return ai_state;
}

void Ai::setState(int state) {
	ai_state = state;
}

int Ai::getNextBeacon(void) {
    return nextBeaconToVisit;
}

void Ai::seenBeacon(int id) {

	if (beaconsSeen[id] == 0) {
		numBeaconsSeen ++;
		beaconsSeen[id] = 1;
	}
}

int Ai::getNumBeaconsSeen(void) {
	return numBeaconsSeen;
}

void Ai::visitedBeacon(void) {
    nextBeaconToVisit ++;
}

void Ai::nextpointCallback(const geometry_msgs::Point::ConstPtr& p) {
	//std::cout << "We get " << p->x << " with state " << ai_state <<std::endl;
    if (p->x != BAD_POINT && ai_state < EXPLORATION) {
		geometry_msgs::Point q;
		q.x = p->x;
		q.y = p->y;
		base_pub.publish(q);
        //std::cout << "A* to point " << q.x << ", " << q.y << std::endl;
	} else {
		ai_state = EXPLORATION;
	}
}

// Right wall follower for the exploration stage
void Ai::laserCallback(const sensor_msgs::LaserScan::ConstPtr& scan) {
	if (Ai::getState() != EXPLORATION) {
		return;
	}

    // Get the smallest range in the angle region to the right
    float minDistRight = INIT_MAX;
    for (int i = MIN_RIGHT_ANGLE; i < MAX_RIGHT_ANGLE; i++) {
        if (std::isfinite(minDistRight) && minDistRight > scan->ranges[i]) {
            minDistRight = scan->ranges[i];
        }
    }

    float minDistFront = INIT_MAX;
    for (int i = MIN_FRONT_ANGLE; i < MAX_FRONT_ANGLE; i++) {
        if (std::isfinite(minDistFront) && minDistFront > scan->ranges[i]) {
            minDistFront = scan->ranges[i];
        }
    }

    // Check our minimum wall distance and send appropriate movement
    geometry_msgs::Twist vel_msg;
    vel_msg.linear.x = NORM_DRIVE_SPEED;
    vel_msg.angular.z = 0;

    //if (minDistRight < TOO_FAR_OUT) {
        vel_msg.linear.x = NORM_DRIVE_SPEED;
        vel_msg.angular.z = fmax(-0.8,(0.35-minDistRight)*10);
    //}

    if (minDistFront < MIN_FRONT_DIST) {
        vel_msg.linear.x = LOW_DRIVE_SPEED;
        vel_msg.angular.z = HARDCORE_TURN;
    }

    move_pub.publish(vel_msg);
}


// Main AI lives here
int main(int argc, char **argv)
{
    ros::init(argc, argv, "RightWall");
	ros::NodeHandle n;

	Ai bot(n);

	// Get the beacons from the params
	XmlRpc::XmlRpcValue beaconsCfg;
	n.getParam("/ai/beacons", beaconsCfg);
	try {
	    std::cout << "adding beacons\n";
		int i = 0;
		int beaconNum = 1;
		do {
			char beaconName[256];
			sprintf(beaconName, "beacon%d", i);
			//sprintf(beaconName, "beacons");
			if (!beaconsCfg.hasMember(beaconName))
				break;

			XmlRpc::XmlRpcValue beaconCfg = beaconsCfg[std::string(beaconName)];
			if (!(beaconCfg.hasMember("top") && beaconCfg.hasMember("bottom")))
				continue;


	        std::string top = (std::string)beaconCfg[("top")];
	        std::string bottom = (std::string)beaconCfg["bottom"];

	        map_beacon b;

	        // Get beacon id from the colours
	        int id = 0;
	        if (top == STRING_PINK) {
	            id = PINK*UPPER_MULTIPLIER;
	        } else if (top == STRING_YELLOW) {
	            id = YELLOW*UPPER_MULTIPLIER;
	        } else if (top == STRING_BLUE) {
	            id = BLUE*UPPER_MULTIPLIER;
	        } else if (top == STRING_GREEN) {
	            id = GREEN*UPPER_MULTIPLIER;
	        }

	        if (bottom == STRING_PINK) {
	            id += PINK;
	        } else if (bottom == STRING_YELLOW) {
	            id += YELLOW;
	        } else if (bottom == STRING_BLUE) {
	            id += BLUE;
	        } else if (bottom == STRING_GREEN) {
	            id += GREEN;
	        }

	        b.id = id;
            std::cout << "Adding beacon with id " << b.id << " in order " << beaconNum << std::endl;
			bot.beaconVisitOrder.insert(std::map<int, int>::value_type(b.id, beaconNum));
			beaconNum ++;
		} while ((++i) != 0);

		// Add beacon list to the global or class variable here

	} catch (XmlRpc::XmlRpcException& e) {
		ROS_ERROR("Unable to parse beacon parameter. (%s)", e.getMessage().c_str());
	}

	//std::cout << "We got to here" << std::endl;


	// Now for real AI
  // This loop holds the main state-based AI - the states are defined at the
  // top of the file and are named after what they are searching for
	while (bot.getState() != DONE) {

		// MAGIC HERE
    // If we haven't seen the 4 beacons we want, explore
		if (bot.getNumBeaconsSeen() < NUM_BEACONS) {
			bot.setState(EXPLORATION);
    // We've seen 4 beacons. Is the beacon we're searching for still there?
		} else if (bot.getNextBeacon() < EXPLORATION) {
			//std::cout << "Go to beacon" << std::endl;
			bot.setState(bot.getNextBeacon());
			// get the next beacon number that we need to visit, and get its id
			map_beacon nextBeacon;
			nextBeacon.id = -1;
			int next = bot.getNextBeacon();
			std::map<int, map_beacon>::iterator it;
			for (it = bot.beacon_list.begin(); it != bot.beacon_list.end(); ++it) {
				if (bot.beaconVisitOrder.count(it->first) != 0) {
					if (bot.beaconVisitOrder.at(it->first) == next) {
						nextBeacon = it->second;
					}
				}
			}

      // somehow we couldn't find the beacon that we wanted, in the Array. Continue
			if (nextBeacon.id == -1) {
				continue;
			}

      // Got the beacon to search for. If we aren't exploring, send off the point to the base
			geometry_msgs::Point goal;
			goal.x = nextBeacon.x;
			goal.y = nextBeacon.y;
			//std::cout << "Going to beacon " << nextBeacon.id << " at " << goal.x << ", " << goal.y << std::endl;
      if (bot.getState() != EXPLORATION)
			    bot.endpoint_pub.publish(goal);

		} else {
      // We're done
			bot.setState(DONE);
			std::cout << "We're done!" << std::endl;
		}
		ros::spinOnce();

	}

  // We've found everything. Twirl on the spot, but ironically
	geometry_msgs::Twist lol;
	lol.angular.z = 5;
	bot.move_pub.publish(lol);


    ros::spin();
    return 0;
}

/*
 * Beacon callback
 * Transforms the beacon to map coordinates and stores it
 * If a duplicate beacon is spotted, the old one is replaced
 */
void Ai::beaconCallback(const ai::beacon_msg::ConstPtr& beaconRef){
    map_beacon b;
    geometry_msgs::Point base_point;
    geometry_msgs::Point map_point;
    //float range = beaconRef - 0.15;

// Dan's old range stuff
	/*std::cout << "range: " << range << std::endl;
	std::cout << "angle: " << beaconRef->angle << std::endl;
    //some trig
    base_point.x = range*cos(beaconRef->angle);
    base_point.y = range*sin(beaconRef->angle);
    base_point.z = 0;*/

	base_point.x=0;
	base_point.y=0;
	base_point.z=0;

    map_point = transformBaseToMap(base_point);

    b.id = beaconRef->id;
    b.x = map_point.x;
    b.y = map_point.y;

	if(beaconRef->range<1){
		//insert if not present, replace if present
		if(beacon_list.count(b.id)){
		    beacon_list.erase(b.id);
		}
		beacon_list.insert(std::map<int, map_beacon>::value_type(b.id, b));
		std::cout << "Beacon stored: id=" << b.id << " x=" << b.x << " y=" << b.y << "\n";
	}



    // Check if the one we have seen is the next to visit
    // First ensure that we need to see this beacon
    if (Ai::beaconVisitOrder.count(b.id) > 0) {
		Ai::seenBeacon(b.id);
		if (Ai::beaconVisitOrder.at(b.id) == Ai::getNextBeacon()) {
		    std::cout << "Beacon ID: " << b.id << "\tat order " << Ai::beaconVisitOrder.at(b.id) << "\tat range " << beaconRef->range << std::endl;
		    // If so, visit the beacon, and remove it from the list
		    // But only if we are close enough
		    std::cout << "Visiting " << b.id << std::endl;
		    if (beaconRef->range < RANGE_THRESHOLD) {
		        Ai::visitedBeacon();
		        Ai::beaconVisitOrder.erase(b.id);
		        std::cout << "Visited " << b.id << std::endl;
		    }
		}
    }
}


/*
 * Transform a base-relative point into a map-relative point
 */

geometry_msgs::Point Ai::transformBaseToMap(geometry_msgs::Point p) {
    geometry_msgs::PointStamped robot_point;
    geometry_msgs::PointStamped map_point_stamped;
    robot_point.header.frame_id = "/base_link";

    // get latest transform
    robot_point.header.stamp = ros::Time::now();

    // set the point coordinates
    robot_point.point.x = p.x;
    robot_point.point.y = p.y;
    robot_point.point.z = 0;

    // get the transform
    try {
        tf_listener_.waitForTransform("/base_link", "/map", robot_point.header.stamp, ros::Duration(1.5));
        tf_listener_.transformPoint("/map", robot_point, map_point_stamped);
    }
    catch (tf::TransformException& ex) {
        ROS_ERROR("%s\n", ex.what());
    }

    // Changed StampedPoint to just a normal Point
    geometry_msgs::Point map_point;
    map_point.x = map_point_stamped.point.x;
    map_point.y = map_point_stamped.point.y;

    return map_point;
}

/*
 * Gets a list of all seen beacons
 */
 /*
std::vector<map_beacon> Ai::getBeaconPoints() {
    std::vector<map_beacon> list;
    map_beacon p;
    for(std::vector<map_beacon>::iterator it = beacon_list.begin(); it != beacon_list.end(); ++it){
        p.x = it->second.x;
        p.y = it->second.y;
        p.id = it->second.id;
        list.push_back(p);
    }
    return list;
}
*/
