/*
 * beacons.cpp
 *
 *  Created on: 25/08/2013
 *      Author: rescue
 */


#include <assign1/beacons.hpp>
#include <ros/ros.h>
#include <XmlRpcException.h>
#include <string>
#include <hugbot/Beacon.h>

namespace comp3431 {

Beacons::Beacons() {
	ros::NodeHandle nh;
	XmlRpc::XmlRpcValue beaconsCfg;

	nh.getParam("/beacons", beaconsCfg);
	try {
		// list of beacons to pass on to the AI
		std::vector<beacon> beacons;
		int i = 0;
		do {
			char beaconName[256];
			sprintf(beaconName, "beacon%d", i);
			if (!beaconsCfg.hasMember(beaconName))
				break;

			XmlRpc::XmlRpcValue beaconCfg = beaconsCfg[std::string(beaconName)];
			beacon b;
			if (!(beaconCfg.hasMember("top") && beaconCfg.hasMember("bottom")))
				continue;

			std::string top = (std::string)beaconCfg[("top")];
			std::string bottom = (std::string)beaconCfg[("bottom")];

			// Set up the beacon structs
			int id = 0;
			beacon b;
			if (top == STRING_PINK) {
				id = UPPER_MULTIPLIER*PINK;
			} else if (top == STRING_YELLOW) {
				id = UPPER_MULTIPLIER*YELLOW;
			} else if (top == STRING_BLUE) {
				id = UPPER_MULTIPLIER*BLUE;
			} else if (top == STRING_GREEN) {
				id = UPPER_MULTIPLIER*GREEN;
			}

			if (bottom == STRING_PINK) {
				id += PINK;
			} else if (bottom == STRING_YELLOW) {
				id += YELLOW;
			} else if (bottom == STRING_BLUE) {
				id += BLUE;
			} else if (bottom == STRING_GREEN) {
				id += GREEN;
			}

			b.id = id;

			beacons.push_back(b);
		} while ((++i) != 0);

		// Now pass on the vector of beacons to the AI

	} catch (XmlRpc::XmlRpcException& e) {
		ROS_ERROR("Unable to parse beacon parameter. (%s)", e.getMessage().c_str());
	}
}

} // namespace comp3431
