// a* imp for RSA

// TODO
// integrate cost map stuff from nate
// create some simple map traversal functions
// change the dim calc for the rawmap data


// make sure that we have camera time on recently explored sections
// limit our search to a radius around bot

#include "std_msgs/String.h"
#include <sstream>
#include <iostream>
#include <queue>
#include <cmath>
#include <cstdlib>

#define COST_STAMP_SIZE 7
#define COST_STAMP_MAX 50

// adjust the below to update map size
#define MAP_WIDTH 30
#define MAP_HEIGHT 30

// this is a generic cell - a cell in the 2d array

class Score{

    Score(double newG, double newF){
        G = newG;
        F = newF;
    }

    double getGScore(void){
        return G;
    }

    double getFScore(void){
        return F;
    }

    double getTotal(void){
        return G+F;
    }

    double G = 0;
    double F = 0;
};

class Cell{
    public:
        // constructor
        Cell(){
            x=0;
            y=0;
            cost=0;
        }

        Cell(int new_x, int new_y, int new_cost){
            x = new_x;
            y = new_y;
            cost = new_cost;
        }

        // overload the < operator for PQ order
        bool operator<(const Cell& rhs) const{
            return cost >= rhs.getCost();
        }

        // overload the == operator for PQ order
        bool operator==(const Cell& rhs) const{
            return (rhs.x == x && rhs.y==y);
        }

        // equals method for comparing our location
        bool equals(const Cell &c){
            return (c.x == x && c.y==y); 
        }

        void set_x(int new_x) {x = new_x;}
        void set_y(int new_y) {y = new_y;}
        
        int get_x() const {return x;}
        int get_y() const {return y;}

        void setCost(int newCost){ cost = newCost;}
        int getCost() const { return cost;}

        void print()const{
            std::cout << "(" << x << ", " << y << "):" << cost;
        }

        int x;
        int y;
        int cost;
};

// need to update to incorporate nates 2d code
class Map{
    public: 
        //constructor - take in the costmap and its dimensions. note: x and y dim should be equal (check) 

        // constructor for 2d map. takes in the 1D
        Map( std::vector<int> rawMap){
            int mapSize_2D = MAP_WIDTH;
            //std::cout << mapSize_2D << std::endl;
            costMap.resize(mapSize_2D);

            costMap.resize(mapSize_2D);
            
            // resize the elements of the 2d map - resize every sub vector
            for (int i = 0; i < mapSize_2D; i++){
                costMap[i].resize(mapSize_2D);
            }


            //copy the elements over from 1d to 2d
            for (unsigned int i = 0; i < rawMap.size(); i++){
                 int row = i / mapSize_2D;
                 int col = i % mapSize_2D;
                 //std::cout << "row: "<< row << std::endl;
                 //std::cout << "col: "<< col << std::endl;

                 costMap[row][col] = rawMap[i];
            }

            //costMap = rawMap;
            dim = mapSize_2D;
            // do rawMap to costmap calc here.
        }

        // get cost from 1D vector - need to incorporate into 2D
        // will not throw exception if not correctly called - fix?
        int getCost(int x, int y){
            return costMap.at(x).at(y);
        }
        
        bool inMap(int x, int y){
            return ( ( x >= 0 ) && ( x < dim ) && ( y >= 0) && ( y < dim) );
        }

        // return a set of all adjacent cells
        std::vector<Cell> get_adjacent(Cell current_pos){
            std::vector<Cell> adjacent;
                //std::cout << "checking:"; 
                //current_pos.print();
            // iterate through adjacent cells, add them to the adjacent vector
            for(int j = (current_pos.get_y()-1); j <= (current_pos.get_y()+1); ++j ){
                for(int i = (current_pos.get_x()-1); i <= (current_pos.get_x()+1); ++i ){
                    //std::cout<< "checking cell:";
                    if( inMap(i, j)){
                        adjacent.push_back(Cell(i,j, getCost(i,j)) ); // create a new access function
                        //Cell(i,j,getCost(i,j)).print();
                        //std::cout<<"--->adjacent"<<std::endl;
                    }else{
                        //std::cout<<"---X not-adjacent"<<std::endl;
                    }
                }
            }
            return adjacent;
        }

        
        //std::vector<int> costMap;
        std::vector<std::vector<int>> costMap;
        int dim;
};

// this is a state - these will be the basis of our search 
class State{
    public:

        // default constructor
        State(){
            pos.set_x(0);
            pos.set_y(0);
        }
        
        // test constructor
        State(double newGScore, double newFScore,  int x, int y){
            gScore = newGScore;// + newFScore;
            
            //std::cout << "newFScore: " << newFScore << std::endl;
            
            fScore = newFScore;
            
            
            //std::cout << "fScore: " << fScore << std::endl;
            pos.setCost(newGScore + newFScore);
            pos.set_x(x);
            pos.set_y(y);
        }

        // copy constructor
        State(const State &parent){
            visited = parent.visited;
            pos = parent.pos;

            gScore = parent.getGScore();
            
            //fScore = parent.getFScore();
            
        }

        // overload the < operator for PQ order
        bool operator<(const State& rhs) const{
            return getTotalCost() > rhs.getTotalCost();
        }

        // overload the == operator
        bool equals( const State& rhs ){
            return ( (pos.get_x() == rhs.pos.get_x()) && (pos.get_x() == rhs.pos.get_x()) );
        }


        // setGscore
        void setGScore(double newScore){
            gScore = newScore;
        }
        
        // setFscore
        void setFScore(double newScore){
            gScore+=newScore;
            //fScore = newScore;
        }

        // updateGscore
        void updateGScore(double newScore){
            gScore += newScore;
        }

        // updateGscore
        void updateFScore(double newScore){
            this->fScore += newScore;
        }

        // setGscore
        double getGScore(void) const{
            return this->gScore;;
        }
        // setFscore
        double getFScore(void) const{
            return this->fScore;
        }

        void setPosCell(Cell newPos){
            pos.set_x(newPos.get_x());
            pos.set_y(newPos.get_x());
        }

        void setPos(int new_x, int new_y){
            pos.set_x(new_x);
            pos.set_y(new_y);
        }

        Cell getPos(void) const{
            return pos;
        }

        void updateVisited(Cell newPos){
            visited.push_back(newPos);
            pos = newPos;
        }

        // check if a cell has been visited.
        bool hasVisited(Cell target){
            
            for(auto previous : visited){
                
                if(previous.equals(target)){
                    //std::cout << "have been here before!"<< std::endl;
                    return true;
                }else{
                    //std::cout << "have not been here before!"<< std::endl;
                }
            }
            return false;
        }

        void printVisited(){
            for(Cell pos: visited){
                pos.print();
            }
        }

        double getTotalCost()const {
            return static_cast<double>(gScore) + fScore;
        }



        double gScore = 0;
        double fScore = 0;
        Cell pos;
        std::vector<Cell> visited;

        
};


void testPQCell();
void testPQState();

std::vector<int> genMap(void){

    // MAP PROFILES - change as need be

    std::vector<int> map1 = 
    {
        0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        100,100,100,100,100,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,100,100,100,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,100,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,100,100,100,100,100,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        100,100,100,100,100,100,100,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,100,100,100,100,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,100,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,-0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        100,100,100,100,100,100,100,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    };

    std::vector<int> map2 = 
    {   1,1,1,
        1,1,1,
        1,1,1
    };

    std::vector<int> map3 = 
    {   1, 1, 1,1,1,1,
        1, 1, 1,1,1,1,
        1, 1,1,1,1,1,
        1,1,1,1,1,1,
        1,1,1,1,1,1,
        1, 1,  1, 1,1,1
    };

    return map1;

}


double getEuclideanDistance(Cell current, Cell goal){
    return (current.get_x()-goal.get_x())*(current.get_x()-goal.get_x())
        +( (current.get_y()-goal.get_y())*(current.get_y()-goal.get_y()));
}

//std::vector<geometry_msgs/Point> a_star(geometry_msgs::Point current_pos, geometry_msgs::Point goal_pos){
std::vector<Cell> a_star(Cell current_pos, Cell goal_pos, Map costMap){
        

        // PQ of states - this will compare based on cost
        std::priority_queue<State> frontier;

        std::cout<< "Creating initial state." <<std::endl;
        // create initial state at our current pos 
        int expandedCells = 0;
        

        int initialScore = current_pos.getCost();
        State initialState;
        initialState.updateVisited(current_pos);
        initialState.setGScore(initialScore);
        initialState.setFScore(getEuclideanDistance(current_pos, goal_pos));
        
        //std::cout << "Initial visited: " << std::endl;
        //initialState.printVisited();

        State current;
        //initialState.setFScore(initialState.getGScore() + 0);
        
        frontier.push(initialState);

        // while our priority queue still has states
        while (!frontier.empty()){
            // PQ uses the comparator implementation in State class 
            
            // pop and remove the first element from the priority queue and call it 'current'
            current =frontier.top();

        //    std::cout<<"current visited:" << std::endl;
        //   current.printVisited();
            
        //    std::cout<<"current pos:" << std::endl;
        //    current.getPos().print();

            frontier.pop();
        //    std::cout << "Frontier size: " << frontier.size() << std::endl;
    
            // check for goal state
            if(current.pos.equals(goal_pos)){
                std::cout << "Goal found." << std::endl;
                std::cout << "expanded cells: "<< expandedCells << std::endl;
                std::cout << "Gscore: "<< current.getGScore() << std::endl;
                return current.visited;
            }
            expandedCells++;
            
            //current.getPos().print();
            //std::cout << std::endl;

            // generate successor states
            for(Cell tempCell: costMap.get_adjacent(current.getPos())){
                
                // create next state - copy paramters from parent
                State successor = State(current);
                
                // skip un-traversable terrain or previously visited cells
                if(tempCell.getCost() == -1 || current.hasVisited(tempCell)){
                    continue;
                }

                successor.updateVisited(tempCell);
                // update state visited list and set current position
                // update G and F scores
                successor.updateGScore(tempCell.getCost());
                
                //std::cout<<"euclid: "<< getEuclideanDistance(tempCell, goal_pos) << std::endl; 
                
                //successor.setFScore(getEuclideanDistance(tempCell, goal_pos));
                successor.setFScore(getEuclideanDistance(tempCell, goal_pos));
                
                //successor.updateFScore(tempCell.getCost());
                //std::cout << "successor.totalScore: ";
                //successor.getPos().print();
                //std::cout << ": " <<successor.getTotalCost()<< std::endl;
                
                
                

                // push new state to frontier
                frontier.push(successor);

            
            }      
        }

        
        std::vector<Cell> dummy;
        dummy.push_back(Cell(-1,-1,0));
        // this will be returned if no goal is found
        std::cout << "Goal not found."<< std::endl;
        return dummy;       

}


std::vector<int> genCostMap(std::vector<int> hectorMap){

    // create cost stamp and initialise
    uint8_t cost_stamp[COST_STAMP_SIZE][COST_STAMP_SIZE];
    int32_t i,j;
    int centre = COST_STAMP_SIZE/2;
    for(i=0;i<COST_STAMP_SIZE;i++){
        for(j=0;j<COST_STAMP_SIZE;j++){
            cost_stamp[i][j] = fmax(0,COST_STAMP_MAX*(1 - sqrt((i-centre)*(i-centre)+(j-centre)*(j-centre))/(COST_STAMP_SIZE/2)));
        }
    }

    std::vector<int> newMap = hectorMap;

    // MAP OPERATIONS
    // ensure probabilities range is set
    int width = MAP_WIDTH;
    int height = MAP_HEIGHT;
    //int8_t costMap[height][width];
    //costMap = newMap.data;
    //memcpy(mapPtr, newMap.data, height*width);
    int32_t x,y;
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if(newMap[i*width+j]==100){
                for(x=i-COST_STAMP_SIZE/2;x<i+COST_STAMP_SIZE/2;x++){
                    for (y = j - COST_STAMP_SIZE / 2; y < j + COST_STAMP_SIZE / 2; y++){
                        if((x>=0&&x<height)&&(y>=0&&y<width)){
                            newMap[x*width+y] = fmax(newMap[x*width+y],cost_stamp[x-i+COST_STAMP_SIZE/2][y-j+COST_STAMP_SIZE/2]);
                        }
                    }
                }
            }
            else if (newMap[i*width + j] == -1){
                newMap[i*width + j] = 0;
            }
        }
    }
    for (i = 0; i < height; i++){
        for (j = 0; j < width; j++){
            if (newMap[i*width + j] == -1){
                newMap[i*width + j] = 0;
            } else if (newMap[i*width + j] == 100){
                newMap[i*width + j] = -1;
            }
        }
    }
    return newMap;

}

void printMap(Map map){
    for(int i=0;i<map.dim;i++){
        for(int j=0;j<map.dim;j++){
            printf("%3d ",map.costMap[i][j]);
        }
    printf("\n");
    }
}

void printPath(std::vector<Cell> path){
    for(Cell pos : path){
        pos.print();
        std::cout << std::endl;
    }
    std::cout<<std::endl;
}

void printQueue(std::priority_queue<Cell> &PQ){
    while(!PQ.empty()){
        PQ.top().print();
        PQ.pop();
    }
}

void printStateQueue(std::priority_queue<State> &PQ){
    while(!PQ.empty()){
        PQ.top().getPos().print();
        std::cout << "->  "<< PQ.top().getTotalCost() <<std::endl;
        std::cout << "-->Gscore: " << PQ.top().getGScore()<< std::endl;
        std::cout << "-->Fscore: " << PQ.top().fScore << std::endl;//PQ.top().getFScore()<< std::endl;
        PQ.pop();
    }
}

void printPathMap(Map map, std::vector<Cell> path){
    int count = 0;
    Map newMap = map;
    for(auto element : path){
        newMap.costMap[element.get_x()][element.get_y()] = count; 
        count++;
    }

    printMap(newMap);
}

int main(int argc, char* argv[]){

    // create a dummy map vector
    std::vector<int> hectorMap = genMap();

    // convert that vector to costmap
    std::vector<int> costMap = genCostMap(hectorMap);

    // create map object: convert costmap to 2d vector
    Map searchMap = Map(costMap);
    std::cout<<"Map"<<std::endl;
    

    printMap(costMap);
    

    // create new cell at 0,0 (y,x)
    Cell start(0,0, searchMap.getCost(0,0) );
    Cell goal(6,17, searchMap.getCost(6,17) );



    std::cout<<"Start: ";
    start.print();
    std::cout << std::endl;
    
    std::cout<<"Goal: ";
    goal.print();
    std::cout << std::endl;


    std::vector<Cell> path = a_star(start, goal, searchMap);


    std::cout << std::endl;
    std::cout << "Path to goal:" << std::endl;
    printPath(path);

  

    //PQ TEST
//    testPQState();

    


    //std::cout<< "eDist: ";
    //start.print(); 
    //std::cout << "-> ";
    //goal.print(); 
    //std::cout<< ": " << getEuclideanDistance(start, goal) << std::endl;



    printPathMap(searchMap, path);


    // create a new start node - can be anywhere that is valid
    //Cell start(0,0,1);
    //Cell end()
    return EXIT_SUCCESS;
}


void testPQCell (){
/*
    std::priority_queue<Cell> PQ;
    PQ.push(Cell(1,1, searchMap.getCost(1,1)));
    PQ.push(Cell(1,2, searchMap.getCost(1,2)));
    PQ.push(Cell(0,3, searchMap.getCost(0,3)));
    PQ.push(Cell(2,3, searchMap.getCost(2,3)));
    PQ.push(Cell(3,0, searchMap.getCost(3,0)));
    PQ.push(Cell(3,2, searchMap.getCost(3,2)));
    PQ.push(Cell(0,0, searchMap.getCost(0,0)));
    PQ.push(Cell(2,1, searchMap.getCost(2,1)));
    PQ.push(Cell(1,3, searchMap.getCost(1,3)));
    PQ.push(Cell(0,2, searchMap.getCost(0,2)));
    PQ.push(Cell(3,1, searchMap.getCost(3,1)));
    PQ.push(Cell(0,1, searchMap.getCost(0,1)));
    PQ.push(Cell(2,0, searchMap.getCost(2,0)));
    PQ.push(Cell(2,2, searchMap.getCost(2,2)));
    PQ.push(Cell(3,3, searchMap.getCost(3,3)));
    PQ.push(Cell(1,0, searchMap.getCost(1,0)));

    printQueue(PQ);
    printMap(searchMap);
    std::cout<< "(0,0): " <<searchMap.getCost(0,0) << std::endl;
    std::cout<< "(0,1): " <<searchMap.getCost(0,1) << std::endl;
    std::cout<< "(0,2): " <<searchMap.getCost(0,2) << std::endl;
    std::cout<< "(0,3): " <<searchMap.getCost(0,3) << std::endl;
    std::cout<< "(1,0): " <<searchMap.getCost(1,0) << std::endl;
    std::cout<< "(1,1): " <<searchMap.getCost(1,1) << std::endl;
    std::cout<< "(1,2): " <<searchMap.getCost(1,2) << std::endl;
    std::cout<< "(1,3): " <<searchMap.getCost(1,3) << std::endl;
*/
}



void testPQState (){
    std::cout << "testing PQ onstates..."<< std::endl;
    
    std::priority_queue<State> PQ;
    
    State newState0 = State(1,0.1, 0, 1);
    newState0.setFScore(0.1);
    State newState1 = State(1,0.2, 0, 2);
    newState1.setFScore(0.2);
    State newState2 = State(1,0.3, 0, 3);
    newState2.setFScore(0.3);
    State newState3 = State(1,0.4, 0, 4);
    newState3.setFScore(0.4);
    State newState4 = State(1,0.5, 0, 5);
    newState4.setFScore(0.5);
    State newState5 = State(1,0.6, 0, 6);
    newState5.setFScore(0.6);
    State newState6 = State(1,0.7, 0, 7);
    newState6.setFScore(0.7);
    //std::cout << "G: " << newState.getGScore()<< std::endl;
    //std::cout << "F: " << newState.getFScore()<< std::endl;

    
    PQ.push(newState3);
    PQ.push(newState4);
    PQ.push(newState0);
    PQ.push(newState2);
    PQ.push(newState5);
    PQ.push(newState6);
    PQ.push(newState1);
    
    /*
    PQ.push(State(2.0, 2.236, 2, 1));
    PQ.push(State(2.0, 2.236, 1, 2));
    

    PQ.push(State(3.0, 2.0, 2, 0));
    PQ.push(State(3.0, 2.0, 0 ,2));

    PQ.push(State(1.0, 1.0, 1, 0));
    PQ.push(State(1.0, 1.0, 0, 1));
    
    PQ.push(State(1.0, 103.0, 0, 0));

    std::cout<< PQ.size()<<std::endl;

    //printMap(searchMap);
    */
    printStateQueue(PQ);

    /*
    std::cout<< "(0,0): " <<searchMap.getCost(0,0) << std::endl;
    std::cout<< "(0,1): " <<searchMap.getCost(0,1) << std::endl;
    std::cout<< "(0,2): " <<searchMap.getCost(0,2) << std::endl;
    std::cout<< "(0,3): " <<searchMap.getCost(0,3) << std::endl;
    std::cout<< "(1,0): " <<searchMap.getCost(1,0) << std::endl;
    std::cout<< "(1,1): " <<searchMap.getCost(1,1) << std::endl;
    std::cout<< "(1,2): " <<searchMap.getCost(1,2) << std::endl;
    std::cout<< "(1,3): " <<searchMap.getCost(1,3) << std::endl;
    */

}







