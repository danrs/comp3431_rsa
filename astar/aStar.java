	public void aStar(Heuristic heuristic){
		
		// our priority queue for the 'open states'. This is where the 
		PriorityQueue<State> frontier = new PriorityQueue<State>();	// OPEN SET representation
		
		// create initial state. This state is always located at SYDNEY. we DO NOT include the sydney delay time.
		int expandedCities = 0;
		int initialScore = 0;
		State initialState = new State("Sydney");
		initialState.setGScore(initialScore);
		initialState.setFScore(initialState.getGScore()+heuristic.getCostEstimate(initialState, reqFlights, cities));
		
		// create a current state and add to frontier
		State current;
		frontier.add(initialState);
		
		// while our priority queue still has states
		while (!frontier.isEmpty()){
			// PQ uses the comparator implementation in State class	
			
			// pop the first element from the priority queue and call it 'current'
			current = frontier.poll();
	
			// check goal state - if goal state, print out the result.
			if(current.goalState(reqFlights)){
				System.out.println(expandedCities+" nodes expanded");
				
				// remove delay of last city - we do not consider this delay
				current.setGScore(current.getGScore()-cities.get(current.getCurrentCity()));
				System.out.println("cost = "+current.getGScore());
				current.showPath();
				return;
			}
			
			// increment our expanded city count
			expandedCities++;
			
			State next;
			
			// generate successor states
			// iterate through our neighbor cities and create a successor state
			for(Connection tempConnection: connections){
		
				if((current.getCurrentCity()).equals(tempConnection.getOrigin())){
					next = new State(tempConnection.getDestination());
					
					// clone the 'visited' array list of the previous state - we will update and add the current connection to this
					next.setVisited(current.getVisitedClone());
					next.addVisisted(tempConnection);
					
					// calculate and populate our G score then F score.
					next.setGScore(current.getGScore()+tempConnection.getWeight()+cities.get(tempConnection.getDestination()));
					next.setFScore(next.getGScore() + heuristic.getCostEstimate(next, reqFlights, cities));

					frontier.add(next);
				}
			}
		}
		return;
	}
