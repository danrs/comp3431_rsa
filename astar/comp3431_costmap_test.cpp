
#include <stdio.h>
#include <cmath>
#include <stdint.h>
#include <string.h>


#define COST_STAMP_SIZE 13
#define COST_STAMP_MAX 50

uint8_t cost_stamp[COST_STAMP_SIZE][COST_STAMP_SIZE];

// MAP PROFILES

int8_t newMap[256] = 
{
  0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,
100,0,0,0,0,0,0,0,0,0,
0,  0,0,0,0,0,100,0,0,0,
0,  0,0,0,0,0,0,0,0,0,0,
0,100,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,100,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,100,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,  0,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,  0,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,  0,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,  0,0,0,0,0,0,0,0,0,0,0,0,
0,0, 0,0,  0,0,0,0,0,0,0,0,0,0,0,0,
0,0,-1,0,0,0,0,0,0,0,0,0,0,100,0,0,
0,0,-1,0,0,0,0,0,0,0,0,0,0,100,0,0,
0,0,-1,0,0,0,0,0,0,0,0,0,0,100,0,0,
0,0,-1,0,0,0,0,0,0,0,0,0,0,  0,0,0,
0,0,-1,0,0,0,0,0,0,0,0,0,0,  0,0,0,
};



int main(){
    int32_t i,j;
    int32_t x,y;
    int centre = COST_STAMP_SIZE/2;
    
    // print out the stamp
    /*
    for(i=0;i<COST_STAMP_SIZE;i++){
        for(j=0;j<COST_STAMP_SIZE;j++){
            cost_stamp[i][j] = fmax(0,COST_STAMP_MAX*(1 - sqrt((i-centre)*(i-centre)+(j-centre)*(j-centre))/(COST_STAMP_SIZE/2)));
            printf("%3d ",cost_stamp[i][j]);
        }
        printf("\n");
    }
    */
    
    // MAP OPERATIONS
    // ensure probabilities range is set
    uint16_t width = 16;
    uint16_t height = 16;
    //uint32_t stampBoundary[2];
    //uint32_t stampSize[2];
    //uint32_t stampStart[2];
    int8_t mapPtr[height][width];
    memcpy(mapPtr,newMap,256);
    for(i=0;i<height;i++){
        for(j=0;j<width;j++){
            if(mapPtr[i][j]==100){
                
                for(x=i-COST_STAMP_SIZE/2;x<i+COST_STAMP_SIZE/2;x++){
                    for(y=j-COST_STAMP_SIZE/2;y<j+COST_STAMP_SIZE/2;y++){
                        if((x>=0&&x<height)&&(y>=0&&y<width)){
                            mapPtr[x][y] = fmax(mapPtr[x][y],cost_stamp[x-i+COST_STAMP_SIZE/2][y-j+COST_STAMP_SIZE/2]);

                        }
                    }
                }

            }

        }
    }
    
    return EXIT_SUCCESS;
}

void printMap(){}
