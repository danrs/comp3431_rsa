#include <cstdlib>
#include <iostream>
#include <vector>
#include <cmath>

int main(int argc, char* argv[]){
    
    std::vector<int> rawMap (100, 0);
    
    /*
    for(unsigned int i = 0; i<rawMap.size(); ++i){
        std::cout << rawMap[i]<< ", ";
    }
    std::cout << std::endl;
    */

    std::vector<std::vector<int> > costMap;

    int mapSize_2D = sqrt(rawMap.size());
    costMap.resize(mapSize_2D);

    costMap.resize(mapSize_2D);
    
    // resize the elements of the 2d map - resize every sub vector
    for (int i = 0; i < mapSize_2D; i++)
    {
        costMap[i].resize(mapSize_2D);
    }


    // copy the elements over from 1d to 2d
    for (unsigned int i = 0; i < rawMap.size(); i++)
    {
        int row = i / mapSize_2D;
        int col = i % mapSize_2D;
        costMap[row][col] = rawMap[i];
    }

    for (int i = 0; i < mapSize_2D; ++i){
        for(int j = 0; j < mapSize_2D; ++j){
            std::cout << costMap[i][j]<<",";
        }
        std::cout<<std::endl;
    }

    return EXIT_SUCCESS;
}
