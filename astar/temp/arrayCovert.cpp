#include <stdio.h>
#include <string.h>

int main()
{
    enum { vertical_dim = 4, horizontal_dim = 5, SZ = vertical_dim*horizontal_dim };

    const int one_d[SZ] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 } ;

    int two_d[vertical_dim][horizontal_dim] ;
    memcpy( two_d[0], one_d, SZ * sizeof(int) ) ;

    for( int i = 0 ; i < vertical_dim ; ++i )
    {
       for( int j = 0 ; j < horizontal_dim ; ++j ) printf( "%d ", two_d[i][j] ) ;
       puts( "" ) ;
    }
}