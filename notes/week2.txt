Week 2 Notes

==Lasers!==
10 Hz
Easily flood the weeny CSE Network (100Mbit)

Common frames of reference:
	- base (robot)
	- base footprint (robot projected onto ground)
	- laser
	- map (the big one you push all your data into)
	
Some maths (seen it before in Robotics)

Frame convention:
Right hand rule
Roll around x
Pitch around y
Yaw around z

Don't need to worry about transformation matrices, except for debug
T^(ypr) = 	[cos(Yaw) -sin(Yaw) 0;		[cos(Pitch)  0  sin(Pitch);	  [1  0           0      ;
			sin(Yaw) cos(Yaw)  0;	*	   0         1    0		  ;	*  0 cos(Roll) -sin(Roll);
			0        0         1]		 -sin(Pitch) 0  cos(Pitch)]	   0 sin(Roll)  cos(Roll)]

Quaternions
Rotation about an axis:
 -Q = cos(theta/2) + x*sin(theta/2)i + y*sin(theta/2)j + *sin(theta/2)k
 Invertible: Q^-1 = w - xi - yj - zk
                         |Q^2|
			Note: Q^2 is usually 1 for us
			
Workspaces:
run make inside it to compile and build the devel directory
The setup.sh and similar scripts are useful for setting up your build dir
Build system is Catkin, based on cmake